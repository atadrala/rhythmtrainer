﻿using NUnit.Framework;
using RhythmTrainer.Models;
using RhythmTrainer.Service;
using RhythmTrainer.Service.Generators;

namespace RhythmTrainer.Tests.Service
{
    [TestFixture]
    public class RoundRuletteTests
    {
        [Test]
        public void GivenTwoGeneratorsWhenPropabilityOfFirstIsOneThenItsPicked()
        {
            // Arrange
            var generatorOne = new FakeGenerator();
            var generatorTwo = new FakeGenerator();
            var sut = new RoundRulette();

            // Act
            sut.SetPropability(generatorOne, 1.0);
            sut.SetPropability(generatorTwo, 0.0);
            var generator = sut.PickGeneratorFor(new BarScafold(new Meter(4, 4)));

            // Assert
            Assert.That(generator, Is.EqualTo(generatorOne));
        }

        [Test]
        public void GivenTwoGeneratorsWhenOnlySecondCanGenerateThenItPickSecond()
        {
            // Arrange
            var generatorOne = new FakeGenerator(false);
            var generatorTwo = new FakeGenerator();
            var sut = new RoundRulette();

            // Act
            sut.SetPropability(generatorOne, 5.0);
            sut.SetPropability(generatorTwo, 5.0);
            var generator = sut.PickGeneratorFor(new BarScafold(new Meter(4, 4)));

            // Assert
            Assert.That(generator, Is.EqualTo(generatorTwo));
        }

        class FakeGenerator : INoteGenerator
        {
            private readonly bool canGenerate;

            public FakeGenerator(bool canGenerate = true)
            {
                this.canGenerate = canGenerate;
            }

            public BarElement GenerateNotes()
            {
                throw new System.NotImplementedException();
            }

            public Rational NoteMeter { get; private set; }
            public bool CanGenerateFor(BarScafold barScafold)
            {
                return canGenerate;
            }
        }
    }
}