﻿using NUnit.Framework;
using RhythmTrainer.Models;
using RhythmTrainer.Service;
using RhythmTrainer.Service.Generators;

namespace RhythmTrainer.Tests.Service.Generators
{
    [TestFixture]
    public class NoneconsecutiveNoteGeneratorTest
    {
        [Test]
        public void GivenNoneconsecutiveNoteGeneratorForEightsWhenBarScafoldEndsWithEightNoteThenItCanNotGenerate()
        {
            // Arrange
            var sut = new NoneconsecutiveNoteGenerator<Eight>(new Rational(1, 8));

            // Act
            var barScafold = new BarScafold(new Meter(4, 4));
            barScafold.AddNote(new Eight());

            // Assert
            Assert.That(sut.CanGenerateFor(barScafold), Is.False);
        }

        [Test]
        public void GivenNoneconsecutiveNoteGeneratorForEightsWhenGeneratingWouldLeftSpaceOnlyForEightThenItCanNotGenerate()
        {
            // Arrange
            var sut = new NoneconsecutiveNoteGenerator<Eight>(new Rational(1, 8));

            // Act
            var barScafold = new BarScafold(new Meter(4, 4));
            barScafold.AddNote(new Half());
            barScafold.AddNote(new Quarter());

            // Assert
            Assert.That(sut.CanGenerateFor(barScafold), Is.False);
        }

        [Test]
        public void GivenNoneconsecutiveNoteGeneratorForEightWhenBarScafoldEndsWithQuarterThenItCanGenerate()
        {
            var sut = new NoneconsecutiveNoteGenerator<Eight>(new Rational(1, 8));

            var barScafold = new BarScafold(new Meter(4, 4));
            barScafold.AddNote(new Half());
            barScafold.AddNote(new Eight());
            barScafold.AddNote(new Quarter());
            Assert.That(sut.CanGenerateFor(barScafold), Is.True);
        }
    }
}
