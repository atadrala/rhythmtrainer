﻿using System;
using System.Collections.Generic;
using Microsoft.Reactive.Testing;
using NUnit.Framework;
using RhythmTrainer.Models;
using RhythmTrainer.Service;

namespace RhythmTrainer.Tests.Service
{
    [TestFixture]
    public class JudgeTests
    {
        [Test]
        public void GivenJudgeAndForQuarterBarWhenRecodingIsPerfectMatchThenResultsHasNoExtraHitsNoMissingNoteAndFourTotalNotesHits()
        {
            // Arranage
            var start = DateTime.Now;
            var meter = new Meter(4, 4);
            var recording = new Recorder();
            var judge = new Judge();
            var metronome = new Metronome(new TestScheduler()) { BeatsPerMinute = 60, MeterCount = meter.Note };
            var musicSheet = new MusicSheet()
            {
                Bars = new List<Bar>
                        {
                            new Bar(new List<BarElement>
                                {
                                    new Quarter(),
                                    new Quarter(),
                                    new Quarter(),
                                    new Quarter(),
                                }, new Meter(4,4))
                        },
                Meter = meter,
            };

            // Act
            recording.RecordTick(new MetronomeTick(0, 0), start);
            recording.RecordTap(start);
            recording.RecordTick(new MetronomeTick(0, 1), start.Add(TimeSpan.FromSeconds(1)));
            recording.RecordTap(start.Add(TimeSpan.FromSeconds(1)));
            recording.RecordTick(new MetronomeTick(0, 2), start.Add(TimeSpan.FromSeconds(2)));
            recording.RecordTap(start.Add(TimeSpan.FromSeconds(2)));
            recording.RecordTick(new MetronomeTick(0, 3), start.Add(TimeSpan.FromSeconds(3)));
            recording.RecordTap(start.Add(TimeSpan.FromSeconds(3)));

            var result = judge.JudgeRecording(recording, metronome, musicSheet);

            // Assert
            Assert.That(result.ExtraHits, Is.EqualTo(0));
            Assert.That(result.MissingNotes, Is.EqualTo(0));
            Assert.That(result.TotalNotes, Is.EqualTo(4));
            Assert.That(result.TotalError, Is.EqualTo(0d).Within(1e-12));
        }

        [Test]
        public void GivenJudgeAndTrippleBarWhenRecodingIsPerfectMatchThenResultsHasNoExtraHitsNoMissingNoteAndThreeTotalNotesHits()
        {
            // Arranage
            var start = DateTime.Now;
            var meter = new Meter(3, 4);
            var recording = new Recorder();
            var judge = new Judge();
            var metronome = new Metronome(new TestScheduler()) { BeatsPerMinute = 60, MeterCount = meter.Note };
            var musicSheet = new MusicSheet()
            {
                Bars = new List<Bar>
                        {
                            new Bar(new List<BarElement>
                                {
                                    new Quarter(),
                                    new Quarter(),
                                    new Quarter(),
                                }, new Meter(3,4))
                        },
                Meter = meter,
            };

            // Act
            recording.RecordTick(new MetronomeTick(0, 0), start);
            recording.RecordTap(start);
            recording.RecordTick(new MetronomeTick(0, 1), start.Add(TimeSpan.FromSeconds(1)));
            recording.RecordTap(start.Add(TimeSpan.FromSeconds(1)));
            recording.RecordTick(new MetronomeTick(0, 2), start.Add(TimeSpan.FromSeconds(2)));
            recording.RecordTap(start.Add(TimeSpan.FromSeconds(2)));

            var result = judge.JudgeRecording(recording, metronome, musicSheet);

            // Assert
            Assert.That(result.ExtraHits, Is.EqualTo(0));
            Assert.That(result.MissingNotes, Is.EqualTo(0));
            Assert.That(result.TotalNotes, Is.EqualTo(3));
            Assert.That(result.TotalError, Is.EqualTo(0d).Within(1e-12));
        }

        [Test]
        public void GivenJudgeAndForMarchBarWhenRecodingIsPerfectMatchThenResultsHasNoExtraHitsNoMissingNoteAndTwoTotalNotesHits()
        {
            // Arranage
            var start = DateTime.Now;
            var meter = new Meter(2, 4);
            var recording = new Recorder();
            var judge = new Judge();
            var metronome = new Metronome(new TestScheduler()) { BeatsPerMinute = 60, MeterCount = meter.Note };
            var musicSheet = new MusicSheet()
            {
                Bars = new List<Bar>
                        {
                            new Bar(new List<BarElement>
                                {
                                    new Quarter(),
                                    new Quarter(),
                                }, new Meter(2, 4))
                        },
                Meter = meter,
            };

            // Act
            recording.RecordTick(new MetronomeTick(0, 0), start);
            recording.RecordTap(start);
            recording.RecordTick(new MetronomeTick(0, 1), start.Add(TimeSpan.FromSeconds(1)));
            recording.RecordTap(start.Add(TimeSpan.FromSeconds(1)));

            var result = judge.JudgeRecording(recording, metronome, musicSheet);

            // Assert
            Assert.That(result.ExtraHits, Is.EqualTo(0));
            Assert.That(result.MissingNotes, Is.EqualTo(0));
            Assert.That(result.TotalNotes, Is.EqualTo(2));
            Assert.That(result.TotalError, Is.EqualTo(0d).Within(1e-12));
        }

        [Test]
        public void GivenJudgeAndForAdvancedMeterBarWhenRecodingIsPerfectMatchThenResultsHasNoExtraHitsNoMissingNoteAndThreeTotalNotesHits()
        {
            // Arranage
            var start = DateTime.Now;
            var meter = new Meter(7, 8);
            var recording = new Recorder();
            var judge = new Judge();
            var metronome = new Metronome(new TestScheduler()) { BeatsPerMinute = 60, MeterCount = meter.Note };
            var musicSheet = new MusicSheet()
            {
                Bars = new List<Bar>
                        {
                            new Bar(new List<BarElement>
                                {
                                    new Half(),
                                    new Quarter(),
                                    new Eight(),
                                }, new Meter(7,8))
                        },
                Meter = meter,
            };

            // Act
            recording.RecordTick(new MetronomeTick(0, 0), start);
            recording.RecordTap(start);
            recording.RecordTick(new MetronomeTick(0, 1), start.Add(TimeSpan.FromSeconds(1)));
            recording.RecordTick(new MetronomeTick(0, 2), start.Add(TimeSpan.FromSeconds(2)));
            recording.RecordTick(new MetronomeTick(0, 3), start.Add(TimeSpan.FromSeconds(3)));
            recording.RecordTick(new MetronomeTick(0, 4), start.Add(TimeSpan.FromSeconds(4)));
            recording.RecordTap(start.Add(TimeSpan.FromSeconds(4)));
            recording.RecordTick(new MetronomeTick(0, 5), start.Add(TimeSpan.FromSeconds(5)));
            recording.RecordTick(new MetronomeTick(0, 6), start.Add(TimeSpan.FromSeconds(6)));
            recording.RecordTap(start.Add(TimeSpan.FromSeconds(6)));

            var result = judge.JudgeRecording(recording, metronome, musicSheet);

            // Assert
            Assert.That(result.ExtraHits, Is.EqualTo(0));
            Assert.That(result.MissingNotes, Is.EqualTo(0));
            Assert.That(result.TotalNotes, Is.EqualTo(3));
            Assert.That(result.TotalError, Is.EqualTo(0d).Within(1e-12));
        }

        [Test]
        public void GivenJudgeAndForQuarterBarWhenRecodingHasThreeHitsThenResultsOneMissingNote()
        {
            // Arranage
            var start = DateTime.Now;
            var meter = new Meter(4, 4);
            var recording = new Recorder();
            var judge = new Judge();
            var metronome = new Metronome(new TestScheduler()) { BeatsPerMinute = 60, MeterCount = meter.Note };
            var musicSheet = new MusicSheet()
            {
                Bars = new List<Bar>
                        {
                            new Bar(new List<BarElement>
                                {
                                    new Quarter(),
                                    new Quarter(),
                                    new Quarter(),
                                    new Quarter(),
                                }, new Meter(4,4))
                        },
                Meter = meter,
            };

            // Act
            recording.RecordTick(new MetronomeTick(0, 0), start);
            recording.RecordTap(start);
            recording.RecordTick(new MetronomeTick(0, 1), start.Add(TimeSpan.FromSeconds(1)));
            recording.RecordTap(start.Add(TimeSpan.FromSeconds(1)));
            recording.RecordTick(new MetronomeTick(0, 2), start.Add(TimeSpan.FromSeconds(2)));
            recording.RecordTap(start.Add(TimeSpan.FromSeconds(2)));
            recording.RecordTick(new MetronomeTick(0, 3), start.Add(TimeSpan.FromSeconds(3)));

            var result = judge.JudgeRecording(recording, metronome, musicSheet);

            // Assert
            Assert.That(result.ExtraHits, Is.EqualTo(0));
            Assert.That(result.MissingNotes, Is.EqualTo(1));
            Assert.That(result.TotalNotes, Is.EqualTo(4));
            Assert.That(result.TotalError, Is.EqualTo(0d).Within(1e-12));
        }

        [Test]
        public void GivenJudgeAndForQuarterBarWhenRecodingHasThreeHitsFirstMissingNoteThenResultsOneMissingNote()
        {
            // Arranage
            var start = DateTime.Now;
            var meter = new Meter(4, 4);
            var recording = new Recorder();
            var judge = new Judge();
            var metronome = new Metronome(new TestScheduler()) { BeatsPerMinute = 60, MeterCount = meter.Note };
            var musicSheet = new MusicSheet()
            {
                Bars = new List<Bar>
                        {
                            new Bar(new List<BarElement>
                                {
                                    new Quarter(),
                                    new Quarter(),
                                    new Quarter(),
                                    new Quarter(),
                                }, new Meter(4,4))
                        },
                Meter = meter,
            };

            // Act
            recording.RecordTick(new MetronomeTick(0, 0), start);
            recording.RecordTick(new MetronomeTick(0, 1), start.Add(TimeSpan.FromSeconds(1)));
            recording.RecordTap(start.Add(TimeSpan.FromSeconds(1)));
            recording.RecordTick(new MetronomeTick(0, 2), start.Add(TimeSpan.FromSeconds(2)));
            recording.RecordTap(start.Add(TimeSpan.FromSeconds(2)));
            recording.RecordTick(new MetronomeTick(0, 3), start.Add(TimeSpan.FromSeconds(3)));
            recording.RecordTap(start.Add(TimeSpan.FromSeconds(3)));

            var result = judge.JudgeRecording(recording, metronome, musicSheet);

            // Assert
            Assert.That(result.ExtraHits, Is.EqualTo(0));
            Assert.That(result.MissingNotes, Is.EqualTo(1));
            Assert.That(result.TotalNotes, Is.EqualTo(4));
            Assert.That(result.TotalError, Is.EqualTo(0d).Within(1e-12));
        }

        [Test]
        public void GivenJudgeAndForQuarterBarWhenRecodingHasFourSmallDelayedHitsThenResuHasTotalErrorNotZeroNoExtraHits()
        {
            // Arranage
            var start = DateTime.Now;
            var smallDelay = 100;
            var meter = new Meter(4, 4);
            var recording = new Recorder();
            var judge = new Judge();
            var metronome = new Metronome(new TestScheduler()) { BeatsPerMinute = 60, MeterCount = meter.Note };
            var musicSheet = new MusicSheet()
            {
                Bars = new List<Bar>
                        {
                            new Bar(new List<BarElement>
                                {
                                    new Quarter(),
                                    new Quarter(),
                                    new Quarter(),
                                    new Quarter(),
                                }, new Meter(4,4))
                        },
                Meter = meter,
            };

            // Act
            recording.RecordTick(new MetronomeTick(0, 0), start);
            recording.RecordTap(start.AddMilliseconds(smallDelay));
            recording.RecordTick(new MetronomeTick(0, 1), start.Add(TimeSpan.FromSeconds(1)));
            recording.RecordTap(start.Add(TimeSpan.FromMilliseconds(1000 + smallDelay)));
            recording.RecordTick(new MetronomeTick(0, 2), start.Add(TimeSpan.FromSeconds(2)));
            recording.RecordTap(start.Add(TimeSpan.FromMilliseconds(2000 + smallDelay)));
            recording.RecordTick(new MetronomeTick(0, 3), start.Add(TimeSpan.FromSeconds(3)));
            recording.RecordTap(start.Add(TimeSpan.FromMilliseconds(3000 + smallDelay)));

            var result = judge.JudgeRecording(recording, metronome, musicSheet);

            // Assert
            Assert.That(result.ExtraHits, Is.EqualTo(0));
            Assert.That(result.MissingNotes, Is.EqualTo(0));
            Assert.That(result.TotalNotes, Is.EqualTo(4));
            Assert.That(result.TotalError, Is.EqualTo(400d).Within(1e-12));
        }

        [Test]
        public void GivenJudgeAndForQuarterBarWhenRecodingHasFourBigDelayedHitsThenResultHasTotalErrorZeroAndFourExtraHits()
        {
            // Arranage
            var start = DateTime.Now;
            var bigDelay = 150;
            var meter = new Meter(4, 4);
            var recording = new Recorder();
            var judge = new Judge();
            var metronome = new Metronome(new TestScheduler()) { BeatsPerMinute = 60, MeterCount = meter.Note };
            var musicSheet = new MusicSheet()
            {
                Bars = new List<Bar>
                        {
                            new Bar(new List<BarElement>
                                {
                                    new Quarter(),
                                    new Quarter(),
                                    new Quarter(),
                                    new Quarter(),
                                }, new Meter(4,4))
                        },
                Meter = meter,
            };

            // Act
            recording.RecordTick(new MetronomeTick(0, 0), start);
            recording.RecordTap(start.AddMilliseconds(bigDelay));
            recording.RecordTick(new MetronomeTick(0, 1), start.Add(TimeSpan.FromSeconds(1)));
            recording.RecordTap(start.Add(TimeSpan.FromMilliseconds(1000 + bigDelay)));
            recording.RecordTick(new MetronomeTick(0, 2), start.Add(TimeSpan.FromSeconds(2)));
            recording.RecordTap(start.Add(TimeSpan.FromMilliseconds(2000 + bigDelay)));
            recording.RecordTick(new MetronomeTick(0, 3), start.Add(TimeSpan.FromSeconds(3)));
            recording.RecordTap(start.Add(TimeSpan.FromMilliseconds(3000 + bigDelay)));

            var result = judge.JudgeRecording(recording, metronome, musicSheet);

            // Assert
            Assert.That(result.ExtraHits, Is.EqualTo(4));
            Assert.That(result.MissingNotes, Is.EqualTo(4));
            Assert.That(result.TotalNotes, Is.EqualTo(4));
            Assert.That(result.TotalError, Is.EqualTo(0d).Within(1e-12));
        }

        [Test]
        public void GivenJudgeAndRestBarWhenRecodingHasPerfectHitsThenResultsNoMissingNoExtra()
        {
            // Arranage
            var start = DateTime.Now;
            var meter = new Meter(4, 4);
            var recording = new Recorder();
            var judge = new Judge();
            var metronome = new Metronome(new TestScheduler()) { BeatsPerMinute = 60, MeterCount = meter.Note };
            var musicSheet = new MusicSheet()
            {
                Bars = new List<Bar>
                        {
                            new Bar(new List<BarElement>
                                {
                                    new Quarter(),
                                    new HalfRest(),
                                    new Quarter(),
                                }, new Meter(4,4))
                        },
                Meter = meter,
            };

            // Act
            recording.RecordTick(new MetronomeTick(0, 0), start);
            recording.RecordTap(start);
            recording.RecordTick(new MetronomeTick(0, 1), start.Add(TimeSpan.FromSeconds(1)));
            recording.RecordTick(new MetronomeTick(0, 2), start.Add(TimeSpan.FromSeconds(2)));
            recording.RecordTick(new MetronomeTick(0, 3), start.Add(TimeSpan.FromSeconds(3)));
            recording.RecordTap(start.Add(TimeSpan.FromSeconds(3)));

            var result = judge.JudgeRecording(recording, metronome, musicSheet);

            // Assert
            Assert.That(result.ExtraHits, Is.EqualTo(0));
            Assert.That(result.MissingNotes, Is.EqualTo(0));
            Assert.That(result.TotalNotes, Is.EqualTo(3));
            Assert.That(result.TotalError, Is.EqualTo(0d).Within(1e-12));
        }


        [Test]
        public void GivenJudgeAndOneNoteWhenRecodingStartsAtSecondBarAndIsPerfectHitsThenResultsNoMissingNoExtra()
        {
            // Arranage
            var start = DateTime.Now;
            var meter = new Meter(4, 4);
            var recording = new Recorder();
            var judge = new Judge();
            var metronome = new Metronome(new TestScheduler()) { BeatsPerMinute = 60, MeterCount = meter.Note };
            var musicSheet = new MusicSheet()
            {
                Bars = new List<Bar>
                        {
                            new Bar(new List<BarElement>
                                {
                                    new Quarter(),
                                    new HalfRest(),
                                    new QuarterRest(),
                                }, new Meter(4,4))
                        },
                Meter = meter,
            };

            // Act
            recording.RecordTick(new MetronomeTick(0, 0), start);
            recording.RecordTick(new MetronomeTick(0, 1), start.Add(TimeSpan.FromSeconds(1)));
            recording.RecordTick(new MetronomeTick(0, 2), start.Add(TimeSpan.FromSeconds(2)));
            recording.RecordTick(new MetronomeTick(0, 3), start.Add(TimeSpan.FromSeconds(3)));
            recording.RecordTick(new MetronomeTick(1, 0), start.Add(TimeSpan.FromSeconds(4)));
            recording.RecordTap(start.Add(TimeSpan.FromSeconds(4)));
            recording.RecordTick(new MetronomeTick(1, 1), start.Add(TimeSpan.FromSeconds(5)));
            recording.RecordTick(new MetronomeTick(1, 2), start.Add(TimeSpan.FromSeconds(6)));
            recording.RecordTick(new MetronomeTick(1, 3), start.Add(TimeSpan.FromSeconds(7)));

            var result = judge.JudgeRecording(recording, metronome, musicSheet);

            // Assert
            Assert.That(result.ExtraHits, Is.EqualTo(0));
            Assert.That(result.MissingNotes, Is.EqualTo(0));
            Assert.That(result.TotalNotes, Is.EqualTo(3));
            Assert.That(result.TotalError, Is.EqualTo(0d).Within(1e-12));
        }

        [Test]
        public void Foo()
        {
            // Arranage
            var start = DateTime.Now;
            var meter = new Meter(4, 4);
            var recording = new Recorder();
            var judge = new Judge();
            var metronome = new Metronome(new TestScheduler()) { BeatsPerMinute = 180, MeterCount = meter.Note };
            var musicSheet = new MusicSheet()
            {
                Bars = new List<Bar>
                        {
                            new Bar(new List<BarElement>
                                {
                                    new TrippleNoteGroup(),
                                    new TrippleNoteGroup(),
                                    new TrippleNoteGroup(),
                                    new TrippleNoteGroup(),
                                }, new Meter(4,4))
                        },
                Meter = meter,
            };
            // Act
            recording.RecordTick(new MetronomeTick(0, 0), start);
            recording.RecordTap(start);
            recording.RecordTap(start.AddSeconds(1.0 / 9));
            recording.RecordTap(start.AddSeconds(2.0 / 9));
            recording.RecordTick(new MetronomeTick(0, 1), start.Add(TimeSpan.FromSeconds(3.0 / 9)));
            recording.RecordTap(start.AddSeconds(3.0 / 9));
            recording.RecordTap(start.AddSeconds(4.0 / 9));
            recording.RecordTap(start.AddSeconds(5.0 / 9));
            recording.RecordTick(new MetronomeTick(0, 2), start.Add(TimeSpan.FromSeconds(6.0 / 9)));
            recording.RecordTap(start.AddSeconds(6.0 / 9));
            recording.RecordTap(start.AddSeconds(7.0 / 9));
            recording.RecordTap(start.AddSeconds(8.0 / 9));
            recording.RecordTick(new MetronomeTick(0, 3), start.Add(TimeSpan.FromSeconds(9.0 / 9)));
            recording.RecordTap(start.AddSeconds(9.0 / 9));
            recording.RecordTap(start.AddSeconds(10.0 / 9));
            recording.RecordTap(start.AddSeconds(11.0 / 9));
            var result = judge.JudgeRecording(recording, metronome, musicSheet);

            // Assert
            Assert.That(result.ExtraHits, Is.EqualTo(0));
            Assert.That(result.MissingNotes, Is.EqualTo(0));
            Assert.That(result.TotalNotes, Is.EqualTo(4));
            Assert.That(result.TotalError, Is.EqualTo(0d).Within(1e-12));
        }
    }
}