﻿using System;
using System.Collections.Generic;
using System.Reactive.Concurrency;
using System.Reactive.Subjects;
using Microsoft.Reactive.Testing;
using NUnit.Framework;
using RhythmTrainer.Service;
using ObservableExtensions = RhythmTrainer.Service.ObservableExtensions;

namespace RhythmTrainer.Tests.Service
{
    [TestFixture]
    public class ObservableExtensionsTests
    {
        [Test]
        public void Foo()
        {
            var testScheduler = new TestScheduler();
            var ticks = new List<long>();

            ObservableExtensions.InvertedInterval(TimeSpan.FromMilliseconds(1000), testScheduler).Subscribe(ticks.Add);
            testScheduler.AdvanceBy(TimeSpan.FromMilliseconds(100).Ticks);
       
            Assert.That(ticks, Is.Not.Empty);
        }

        [Test]
        public void Bar()
        {
            var testScheduler = new TestScheduler();
            var ticks = new List<long>();

            ObservableExtensions
                .InvertedInterval(TimeSpan.FromMilliseconds(1000), testScheduler)
                .EndOn(4)
                .Subscribe(ticks.Add);

            testScheduler.AdvanceBy(TimeSpan.FromMilliseconds(10000).Ticks);

            Assert.That(ticks, Has.Count.EqualTo(4));
        }
        
        [Test]
        public void Bazz()
        {
            var testScheduler = new TestScheduler();
            var ticks = new List<long>();
            var ended=false;
            
            ObservableExtensions
                .InvertedInterval(TimeSpan.FromMilliseconds(1000), testScheduler)
                .EndOn(4)
                .Subscribe(ticks.Add, ()=> ended = true);

            testScheduler.AdvanceBy(TimeSpan.FromMilliseconds(3900).Ticks);

            Assert.That(ticks, Has.Count.EqualTo(4));
            Assert.That(ended, Is.False);
        }

        [Test]
        public void Bazz2()
        {
            var testScheduler = new TestScheduler();
            var ticks = new List<long>();
            var ended = false;

            ObservableExtensions
                .InvertedInterval(TimeSpan.FromMilliseconds(1000), testScheduler)
                .EndOn(4)
                .Subscribe(ticks.Add, () => ended = true);

            testScheduler.AdvanceBy(TimeSpan.FromMilliseconds(4001).Ticks);

            Assert.That(ticks, Has.Count.EqualTo(4));
            Assert.That(ended, Is.True);
        }

        [Test]
        public void Bazz3()
        {
            var testScheduler = new TestScheduler();
            var ticks = new List<long>();
            var ended = false;
            var stop = new Subject<int>();

            ObservableExtensions
                .InvertedInterval(TimeSpan.FromMilliseconds(1000), testScheduler)
                .UntilNextAfter(stop)
                .Subscribe(ticks.Add, () => ended = true);

            testScheduler.AdvanceBy(TimeSpan.FromMilliseconds(1000).Ticks);
            stop.OnNext(1);

            Assert.That(ticks, Has.Count.EqualTo(2));
            Assert.That(ended, Is.False);
        }
    }
}