﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Reactive.Testing;
using NUnit.Framework;
using RhythmTrainer.Models;
using RhythmTrainer.Service;

namespace RhythmTrainer.Tests.Service
{
    [TestFixture]
    public class MetronomeTests
    {
        [Test]
        public void GivenMetronomeWhenTimerTicksThenClickSoundIsPlayed()
        {
            // Arrange
            var testScheduler = new TestScheduler();
            var sut = new Metronome(testScheduler);
            var meter = new Meter(4, 4);
            var bpm = 120;
            var playedTicks = new List<MetronomeTick>();

            // Act
            sut.Subscribe(playedTicks.Add);
            sut.Start(meter, bpm, 4);
            testScheduler.AdvanceBy(TimeSpan.FromMilliseconds(3800).Ticks);
           
            // Assert
            Assert.That(playedTicks.Count(x=>x.Tick == 0), Is.EqualTo(2));
            Assert.That(playedTicks.Count(x=>x.Tick != 0), Is.EqualTo(6));
        }

        [Test(Description =  "|!---!--2|!---!---|")]
        public void GivenMetronomeWhenStartedAndCallPlayedThenTwoBarsArePlayed()
        {
            var testScheduler = new TestScheduler();
            var sut = new Metronome(testScheduler);
            var meter = new Meter(2, 2);
            var bpm = 120;
            var ticks = new List<MetronomeTick>();

            // Act 
            sut.Subscribe(ticks.Add);
            sut.Start(meter, bpm, 2);
           
            testScheduler.AdvanceBy(TimeSpan.FromMilliseconds(700).Ticks);
            sut.Play();
            testScheduler.AdvanceBy(TimeSpan.FromMilliseconds(5000).Ticks);

            // Assert
            Assert.That(ticks, Has.Count.EqualTo(4));
            Assert.That(ticks.Count(t => t.Tick == 0), Is.EqualTo(2));
            Assert.That(ticks.Count(t => t.Tick == 1), Is.EqualTo(2));
            Assert.That(ticks.Count(t => t.Bar == 0), Is.EqualTo(2));
            Assert.That(ticks.Count(t => t.Bar == 1), Is.EqualTo(2));
        }

        [Test]
        public void GivenMetronomeWhenPlayCalledBeforeOneButWithingMarginThenNextTicksAreFirstBar()
        {
            var testScheduler = new TestScheduler();
            var sut = new Metronome(testScheduler);
            var meter = new Meter(2, 2);
            var bpm = 120;
            var ticks = new List<MetronomeTick>();

            // Act
            sut.OnOneMargin = 80;
            sut.Subscribe(ticks.Add);
            sut.Start(meter, bpm, 2);

            testScheduler.AdvanceBy(TimeSpan.FromMilliseconds(980).Ticks);
            sut.Play();
            testScheduler.AdvanceBy(TimeSpan.FromMilliseconds(5200).Ticks);

            // Assert
            Assert.That(ticks, Has.Count.EqualTo(6));
        }

        [Test]
        public void GivenMetronomeWhenPlayCalledJustAfterOneThenNextTicksAreFirstBar()
        {
            var testScheduler = new TestScheduler();
            var sut = new Metronome(testScheduler);
            var meter = new Meter(2, 2);
            var bpm = 120;
            var ticks = new List<MetronomeTick>();

            // Act
            sut.OnOneMargin = 80;
            sut.Subscribe(ticks.Add);
            sut.Start(meter, bpm, 2);

            testScheduler.AdvanceBy(TimeSpan.FromMilliseconds(1020).Ticks);
            sut.Play();
            testScheduler.AdvanceBy(TimeSpan.FromMilliseconds(6000).Ticks);

            // Assert
            Assert.That(ticks, Has.Count.EqualTo(6));
        }

        [Test]
        public void GivenMetronomeWhenPlayCalledBeforeOneBeforeMarginThenNextTicksAreSecondBar()
        {
            var testScheduler = new TestScheduler();
            var sut = new Metronome(testScheduler);
            var meter = new Meter(2, 2);
            var bpm = 120;
            var ticks = new List<MetronomeTick>();

            // Act
            sut.OnOneMargin = 80;
            sut.Subscribe(ticks.Add);
            sut.Start(meter, bpm, 2);

            testScheduler.AdvanceBy(TimeSpan.FromMilliseconds(700).Ticks);
            sut.Play();
            testScheduler.AdvanceBy(TimeSpan.FromMilliseconds(5000).Ticks);

            // Assert
            Assert.That(ticks, Has.Count.EqualTo(4));
        }

        [Test]
        public void GivenMetronomeWhenPlayCalledInsideBarWithinMarginThenNextTicksAreInTheSameBar()
        {
            var testScheduler = new TestScheduler();
            var sut = new Metronome(testScheduler);
            var meter = new Meter(2, 2);
            var bpm = 120;
            var ticks = new List<MetronomeTick>();

            // Act
            sut.OnOneMargin = 80;
            sut.Subscribe(ticks.Add);
            sut.Start(meter, bpm, 2);

            testScheduler.AdvanceBy(TimeSpan.FromMilliseconds(520).Ticks);
            sut.Play();
            testScheduler.AdvanceBy(TimeSpan.FromMilliseconds(5000).Ticks);

            // Assert
            Assert.That(ticks, Has.Count.EqualTo(4));
        }

        [Test]
        public void GivenMetronomeWhenTimerTicksThenClickSoundIsPlayed2()
        {
            // Arrange
            var testScheduler = new TestScheduler();
            var sut = new Metronome(testScheduler);
            var meter = new Meter(4, 4);
            var bpm = 120;
            var ticks = new List<MetronomeTick>();

            // Act
            sut.Subscribe(ticks.Add);
            sut.Start(meter, bpm, 4);
            testScheduler.AdvanceBy(TimeSpan.FromMilliseconds(1999).Ticks);

            // Assert
            Assert.That(ticks, Has.Count.EqualTo(4));
        }

        [Test]
        public void GivenPlayingMetronomeWhenStopedThenMetronomeEndedRaisedFalse()
        {
            var testScheduler = new TestScheduler();
            var sut = new Metronome(testScheduler);
            var ticks = new List<MetronomeTick>();
            var finishedResult = true;

            // Act
            sut.OnOneMargin = 80;
            sut.Subscribe(ticks.Add);
            sut.Start(new Meter(2, 2), 120, 4);
            sut.MetronomEnded.Subscribe( finished => finishedResult = finished);

            testScheduler.AdvanceBy(TimeSpan.FromMilliseconds(520).Ticks);
            sut.Stop();
            testScheduler.AdvanceBy(TimeSpan.FromMilliseconds(5000).Ticks);

            // Assert
            Assert.That(ticks, Has.Count.EqualTo(2));
            Assert.That(finishedResult, Is.False);
        }


        [Test]
        public void GivenPlayingMetronomeWhenPlayedRequesteNumberOnBarsThenMetronomeEndedRaisedTrue()
        {
            var testScheduler = new TestScheduler();
            var sut = new Metronome(testScheduler);
            var ticks = new List<MetronomeTick>();
            var finishedResult = false;

            // Act
            sut.OnOneMargin = 80;
            sut.Subscribe(ticks.Add);
            sut.Start(new Meter(2, 2), 120, 1);
            sut.Play();
            sut.MetronomEnded.Subscribe(finished => finishedResult = finished);
            testScheduler.AdvanceBy(TimeSpan.FromMilliseconds(5000).Ticks);

            // Assert
            Assert.That(ticks, Has.Count.EqualTo(2));
            Assert.That(finishedResult, Is.True);
        }
    }

    public class MockedSoundPlayer : ISoundPlayer
    {
        public void Tap()
        {
            TapsCount++;
        }

        public void Click()
        {
            ClickCount++;
        }

        public void Clack()
        {
            ClackCount++;
        }

        public void Play(MetronomeTick metronomeTick)
        {
            
        }

        public int ClickCount { get; set; }

        public int ClackCount { get; set; }

        public int TapsCount { get; set; }
    }
}