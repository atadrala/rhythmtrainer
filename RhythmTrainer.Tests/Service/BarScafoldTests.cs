﻿using NUnit.Framework;
using RhythmTrainer.Models;
using RhythmTrainer.Service;

namespace RhythmTrainer.Tests.Service
{
    [TestFixture]
    public class BarScafoldTests
    {
        [Test]
        public void GivenBarScafoldForNotEvenMeterWhenAddedMaxCapacityNotesThenBarIsComplete()
        {
            // Arrange
            var barScafold = new BarScafold(new Meter(5, 4));

            // Act
            barScafold.AddNote( new Half());
            barScafold.AddNote( new Half());
            barScafold.AddNote( new Quarter());

            // Assert
            Assert.That(barScafold.IsComplete);
        }

        [Test]
        public void GivenBarScafoldForFourFourMeterWhenAddedFullNoteEquivalentThenBarIsComplete()
        {
            // Arrange
            var barScafold = new BarScafold(new Meter(4, 4));

            // Act
            barScafold.AddNote(new Half());
            barScafold.AddNote(new Quarter());
            barScafold.AddNote(new Quarter());

            // Assert
            Assert.That(barScafold.IsComplete);
        }

        [Test]
        public void GivenBarScafoldForEvenMeterWhenNoNotesAddedThenBarIsNotComplete()
        {
            // Arrange
            var barScafold = new BarScafold(new Meter(4, 4));

            // Act
         
            // Assert
            Assert.That(barScafold.IsComplete, Is.False);
        }

        [Test]
        public void GivenBarScafoldForEvenMeterWhenQuarterIsAddedThenBarIsNotComplete()
        {
            // Arrange
            var barScafold = new BarScafold(new Meter(4, 4));

            // Act
            barScafold.AddNote(new Quarter());

            // Assert
            Assert.That(barScafold.IsComplete, Is.False);
        }
    }
}