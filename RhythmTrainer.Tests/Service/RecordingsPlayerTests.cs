﻿using System;
using System.Collections.Generic;
using Microsoft.Reactive.Testing;
using NUnit.Framework;
using RhythmTrainer.Service;

namespace RhythmTrainer.Tests.Service
{
    [TestFixture]
    public class RecordingsPlayerTests
    {
        [Test]
        public void GivenSigleBarRecordingsWithMetronomeTicksWhenPlayedThenClickAndFourClacksPlayed()
        {
            // Arrange
            var startTime = DateTime.Now;
            var testScheduler = new TestScheduler();
            var recordings = new List<Recorder.RecordEvent>
                {
                    new Recorder.RecordEvent {Bar = 0, Tick = 0, IsMetronomeTick = true, Time = startTime,},
                    new Recorder.RecordEvent {Bar = 0, Tick = 1, IsMetronomeTick = true, Time = startTime.Add(TimeSpan.FromMilliseconds(1000)),},
                    new Recorder.RecordEvent {Bar = 0, Tick = 2, IsMetronomeTick = true, Time = startTime.Add(TimeSpan.FromMilliseconds(2000)),},
                    new Recorder.RecordEvent {Bar = 0, Tick = 3, IsMetronomeTick = true, Time = startTime.Add(TimeSpan.FromMilliseconds(3000)),},
                };
            var soundPlayer = new MockedSoundPlayer();
            var sut = new RecordingsPlayer(soundPlayer, testScheduler);

            // Act
            sut.Play(recordings);
            testScheduler.AdvanceBy(4000);

            //Assert
            Assert.That(soundPlayer.ClackCount, Is.EqualTo(1));
            Assert.That(soundPlayer.ClickCount, Is.EqualTo(3));
        }

        [Test]
        [Ignore]
        public void GivenSigleBarRecordingsWithTwoTappsWhenPlayedThenDelayBetweenPlayedSoundIsEqualToFirstTapTime()
        {
            // Arrange
            var startTime = DateTime.Now;
            var testScheduler = new TestScheduler();
            var recordings = new List<Recorder.RecordEvent>
                {
                    new Recorder.RecordEvent {Bar = 0, Tick = 0, IsMetronomeTick = true, Time = startTime,},
                    new Recorder.RecordEvent {Bar = 0, Tick = 1, IsMetronomeTick = false, Time = startTime.Add(TimeSpan.FromMilliseconds(1342)),},
                };
            var soundPlayer = new MockedSoundPlayer();
            var sut = new RecordingsPlayer(soundPlayer, testScheduler);

            // Act
            sut.Play(recordings);

            //Assert
            //Assert.That(timer.LastDelay.TotalMilliseconds, Is.EqualTo(1342));
        }

        [Test]
        public void GivenSigleBarRecordingsWithFourTappsWhenHitStopAfterSecondThenOnlyTwoTapsPlayed()
        {
            // Arrange
            var startTime = DateTime.Now;
            var testScheduler = new TestScheduler();
            var recordings = new List<Recorder.RecordEvent>
                {
                    new Recorder.RecordEvent {Bar = 0, Tick = 0, IsMetronomeTick = false, Time = startTime,},
                    new Recorder.RecordEvent {Bar = 0, Tick = 1, IsMetronomeTick = false, Time = startTime.Add(TimeSpan.FromMilliseconds(1000)),},
                    new Recorder.RecordEvent {Bar = 0, Tick = 1, IsMetronomeTick = false, Time = startTime.Add(TimeSpan.FromMilliseconds(2000)),},
                    new Recorder.RecordEvent {Bar = 0, Tick = 1, IsMetronomeTick = false, Time = startTime.Add(TimeSpan.FromMilliseconds(3000)),},
                };
            var soundPlayer = new MockedSoundPlayer();
            var sut = new RecordingsPlayer(soundPlayer, testScheduler);

            // Act
            sut.Play(recordings);
            testScheduler.AdvanceBy(4);
            sut.Stop();
            testScheduler.AdvanceBy(4);

            //Assert
            Assert.That(soundPlayer.TapsCount, Is.EqualTo(2));
        }

        [Test]
        public void GivenSigleBarRecordingsWithFiveTappsWhenPlayedThenTapIsPlayedFiveTimes()
        {
            // Arrange
            var startTime = DateTime.Now;
            var testScheduler = new TestScheduler();
            var recordings = new List<Recorder.RecordEvent>
                {
                    new Recorder.RecordEvent {Bar = 0, IsMetronomeTick = false, Time = startTime,},
                    new Recorder.RecordEvent {Bar = 0, IsMetronomeTick = false, Time = startTime.Add(TimeSpan.FromMilliseconds(300)),},
                    new Recorder.RecordEvent {Bar = 0, IsMetronomeTick = false, Time = startTime.Add(TimeSpan.FromMilliseconds(2542)),},
                    new Recorder.RecordEvent {Bar = 0, IsMetronomeTick = false, Time = startTime.Add(TimeSpan.FromMilliseconds(5475)),},
                    new Recorder.RecordEvent {Bar = 0, IsMetronomeTick = false, Time = startTime.Add(TimeSpan.FromMilliseconds(7342)),},
                };
            var soundPlayer = new MockedSoundPlayer();
            var sut = new RecordingsPlayer(soundPlayer, testScheduler);

            // Act
            sut.Play(recordings);
            testScheduler.AdvanceBy(4000);

            //Assert
            Assert.That(soundPlayer.TapsCount, Is.EqualTo(5));
        }
    }
}