﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reactive;
using System.Reactive.Concurrency;
using System.Reactive.Linq;
using Microsoft.Reactive.Testing;
using NUnit.Framework;
using RhythmTrainer.Models;
using RhythmTrainer.Service;

namespace RhythmTrainer.Tests.Service
{
    [TestFixture]
    public class Judge2Tests
    {
        [Test]
        public void Test1()
        {
            // Arrange
            var testScheduler = new TestScheduler();
            var notes = new BarElement[]
            {
                new Quarter(),
                new Eight(),
                new Quarter(),
                new Eight(),
                new Quarter(),
            };
            var noteObservable = notes.ToObservable()
                .DelayNextBy(x => TimeSpan.FromMilliseconds(x.ToMiliseconds(4, 240)), testScheduler)
                .Do(x => Debug.WriteLine(x));
            var taps = new int[] {250, 125, 270, 125, 230}.ToObservable()
                .DelayNextBy(x => TimeSpan.FromMilliseconds(x), testScheduler)
                .Do(x => Debug.WriteLine(x))
                .Select(x => new Tap());

            //Act
            var result = noteObservable.GroupHitsByNote(taps, testScheduler).FlattenNoteGroups();

            var dic = new Dictionary<BarElement, IList<Tap>>();
            result.Subscribe(x => dic[x.Note] = x.Hits);
            testScheduler.AdvanceBy(TimeSpan.FromMilliseconds(5000).Ticks);

            // Assert
            Assert.That(dic, Has.Count.EqualTo(5));
            for (int i = 0; i < 5; i++)
            {
                Assert.That(dic[notes[i]], Has.Count.EqualTo(1));
            }
        }
    
        [Test]
        public void Test2DoubleHit()
        {
            // Arrange
            var testScheduler = new TestScheduler();
            var notes = new BarElement[]
            {
                new Quarter(),
                new Eight(),
                new Quarter(),
                new Eight(),
                new Quarter(),
            };
            var noteObservable = notes.ToObservable()
                .DelayNextBy(x => TimeSpan.FromMilliseconds(x.ToMiliseconds(4, 240)), testScheduler)
                .Do(x => Debug.WriteLine(x));
            var ticks = new int[] {10, 230,20, 115,20, 240,20, 115,20, 220 }.ToObservable()
                .DelayNextBy(x => TimeSpan.FromMilliseconds(x), testScheduler)
                .Do(x => Debug.WriteLine(x))
                .Select(x=>new Tap());

            //Act
            var result = noteObservable.GroupHitsByNote(ticks, testScheduler).FlattenNoteGroups();

            var dic = new Dictionary<BarElement, IList<Tap>>();
            result.Subscribe(x => dic[x.Note] = x.Hits);
            testScheduler.AdvanceBy(TimeSpan.FromMilliseconds(5000).Ticks);

            // Assert
            Assert.That(dic, Has.Count.EqualTo(5));
            for (int i = 0; i < 5; i++)
            {
                Assert.That(dic[notes[i]], Has.Count.EqualTo(2));
            }
        }

        [Test]
        public void Test3Closes()
        {
            var testScheduler = new TestScheduler();
            var ticks = new int[] { 0, 20, 20, 25, 20 }.ToObservable()
                .DelayEachBy(x => TimeSpan.FromMilliseconds(x), testScheduler)
                .Do(x => Debug.WriteLine(x))
                .Select(x=>new Tap());

            var  notesGroup = Observable.Return(new NoteHitGroup(new Quarter(), ticks));
            var closest = notesGroup.SelectClosestHit(testScheduler);
            var list = new List<NoteHit2>();
            closest.Subscribe(list.Add);

            testScheduler.AdvanceBy(TimeSpan.FromMilliseconds(5000).Ticks);

            Assert.That(list, Has.Count.EqualTo(1));
            Assert.That(list[0].Delay, Is.EqualTo(-10).Within(1e-3));
        }

        [Test]
        public void Test3NoClosest()
        {
            var testScheduler = new TestScheduler();
            var ticks = Observable.Empty<Tap>().Delay(TimeSpan.FromMilliseconds(50), testScheduler);

            var notesGroup = Observable.Return(new NoteHitGroup(new Quarter(), ticks));
            var closest = notesGroup.SelectClosestHit(testScheduler);
            var list = new List<NoteHit2>();
            closest.Subscribe(list.Add);

            testScheduler.AdvanceBy(TimeSpan.FromMilliseconds(5000).Ticks);

            Assert.That(list, Has.Count.EqualTo(1));
        }
    }

    public static class ObservableExtensions2
    {    
        private static readonly TimeSpan TimeWindow = TimeSpan.FromMilliseconds(100);
        private static readonly TimeSpan HalfTimeWindow = TimeSpan.FromMilliseconds(50);

        private const int MaxTicks = 2000;

        public static IObservable<NoteHitGroup> GroupHitsByNote(this IObservable<BarElement> notes, IObservable<Tap> taps, IScheduler scheduler)
        {
            var n = notes.Publish().RefCount();
            var t = taps.Publish().RefCount();
        
            return n.GroupJoin(t.Delay(TimeSpan.FromMilliseconds(50), scheduler),
                        b => Observable.Return(Unit.Default).Delay(TimeWindow, scheduler),
                        m => Observable.Return(Unit.Default),
                        (note, hit) => new NoteHitGroup(note, hit));
        }

        public static IObservable<NoteHit2> FlattenNoteGroups(this IObservable<NoteHitGroup> noteHitGroup)
        {
            return noteHitGroup.SelectMany(
                ng => ng.Taps.Buffer(1000).Select(taps => new NoteHit2 {Note = ng.Note, Hits = taps}));
        }

        public static IObservable<NoteHit2> SelectClosestHit(this IObservable<NoteHitGroup> noteHitGroup, IScheduler scheduler)
        {
            return noteHitGroup.Timestamp(scheduler)
                .Select(noteTime =>
                    new
                    {
                        note = noteTime.Value.Note,
                        delay = noteTime.Value.Taps
                            .Timestamp(scheduler)
                            .Select(tapTime => tapTime.Timestamp.Subtract(noteTime.Timestamp.Add(HalfTimeWindow)).TotalMilliseconds)
                            .Buffer(1000)
                            .Take(1),
                    })
                .SelectMany(n =>
                      n.delay.Select(t => new NoteHit2() { Note = n.note, Delay = t.OrderBy(Math.Abs).FirstOrDefault() })
                       .DefaultIfEmpty(new NoteHit2(){Note = n.note, Delay = double.MaxValue}));
        } 
    }

    public class NoteHitGroup
    {
        public NoteHitGroup(BarElement note, IObservable<Tap> taps)
        {
            Note = note;
            Taps = taps;
        }

        public BarElement Note { get; set; }
        public IObservable<Tap> Taps { get; set; }
    }

    public class NoteHit2
    {
        public BarElement Note { get; set; }
        public IList<Tap> Hits { get; set; }
        public double Delay { get; set; }
    }

    public class Tap { }
}