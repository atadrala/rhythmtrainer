﻿using System;
using System.Collections.Generic;
using Microsoft.Reactive.Testing;
using NUnit.Framework;
using RhythmTrainer.Models;
using RhythmTrainer.Service;

namespace RhythmTrainer.Tests.Service
{
    [TestFixture]
    public class MusicPlayerTests
    {
        [Test]
        public void GivenMusicWithFourNotesWhenPlayedThenFourTicksPlayed()
        {
            //Arrange
            var soundPlayer = new MockedSoundPlayer();
            var testScheduler = new TestScheduler();
            var sut = new MusicPlayer(soundPlayer, testScheduler);
            var musicSheet = new MusicSheet()
                {
                    Meter = new Meter(4, 4),
                    Bars = new[]
                        {
                             new Bar(new List<BarElement>
                                 {
                                     new Quarter(),
                                     new Quarter(),
                                     new Quarter(),
                                     new Quarter(),
                                 }, new Meter(4, 4))
                         }
                };

            //Act
            sut.Play(musicSheet, 80);
            testScheduler.AdvanceBy(TimeSpan.FromMilliseconds(8000).Ticks);

            //Assert
            Assert.That(soundPlayer.TapsCount, Is.EqualTo(4));
        }

        [Test]
        public void GivenMusicWithRestWhenPlayedThenThreeTicksPlayed()
        {
            //Arrange
            var soundPlayer = new MockedSoundPlayer();
            var testScheduler = new TestScheduler();
            var sut = new MusicPlayer(soundPlayer, testScheduler);
            var musicSheet = new MusicSheet()
            {
                Meter = new Meter(4, 4),
                Bars = new[]
                        {
                             new Bar(new List<BarElement>
                                 {
                                     new Quarter(),
                                     new Quarter(),
                                     new QuarterRest(),
                                     new Quarter(),
                                 }, new Meter(4, 4))
                         }
            };
            //Act
            sut.Play(musicSheet, 80);
            testScheduler.AdvanceBy(TimeSpan.FromMilliseconds(7000).Ticks);

            //Assert
            Assert.That(soundPlayer.TapsCount, Is.EqualTo(3));
        }

        [Test]
        public void GivenMusicWithTwoBarWhenPlayedThenAllNotesArePlayed()
        {
            //Arrange
            var soundPlayer = new MockedSoundPlayer();
            var testScheduler = new TestScheduler();
            var sut = new MusicPlayer(soundPlayer, testScheduler);
            var musicSheet = new MusicSheet()
            {
                Meter = new Meter(4, 4),
                Bars = new[]
                        {
                             new Bar(new List<BarElement>{new FullNote()}, new Meter(4, 4)),
                             new Bar(new List<BarElement>{new FullNote()}, new Meter(4, 4)),
                         }
            };
            //Act
            sut.Play(musicSheet, 80);
            testScheduler.AdvanceBy(TimeSpan.FromMilliseconds(8000).Ticks);

            //Assert
            Assert.That(soundPlayer.TapsCount, Is.EqualTo(2));
        }

        [Test]
        public void GivenMusicPlaingWhenStopedThenNoMoreNotesIsPlayed()
        {
            //Arrange
            var soundPlayer = new MockedSoundPlayer();
            var testScheduler = new TestScheduler();
            var sut = new MusicPlayer(soundPlayer, testScheduler);
            var musicSheet = new MusicSheet()
            {
                Meter = new Meter(4, 4),
                Bars = new[]
                        {
                             new Bar(new List<BarElement>{new FullNote()}, new Meter(4, 4)),
                             new Bar(new List<BarElement>{new FullNote()}, new Meter(4, 4)),
                         }
            };
            //Act
            sut.Play(musicSheet, 60);
            testScheduler.AdvanceBy(TimeSpan.FromMilliseconds(2500).Ticks);
            sut.Stop();
            testScheduler.AdvanceBy(TimeSpan.FromMilliseconds(3200).Ticks);

            //Assert
            Assert.That(soundPlayer.TapsCount, Is.EqualTo(1));
        }
    }
}