﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using NUnit.Framework;
using RhythmTrainer.Models;
using RhythmTrainer.Service;

namespace RhythmTrainer.Tests.Service
{
    [TestFixture]
    public class AchievementJudgeTests
    {
        [Test]
        public async void GivenAchivementJudgeWhenJudgeResultsThenAllAchivementsAreEvaluated()
        {
            //Arrange
            var achivement1 = new Achievement(null, 1);
            var achivement2 = new Achievement(null, 1);
            var line1 = new AchievementLineReturning(achivement1);
            var line2 = new AchievementLineReturning(achivement2);
            var line3 = new AchievementLineReturning(null);
            var resultsRepo = new ResultRepositoryReturning(new ResultAggregate());
            var judge = new AchievementJudge(resultsRepo, new[] { line1, line2, line3 });

            // Act
            var achivements = await judge.JudgeResult(new Results());

            // Assert
            Assert.That(achivements, Contains.Item(achivement1));
            Assert.That(achivements, Contains.Item(achivement2));
            Assert.That(achivements.All(x=>x != null));
        }

        [Test]
        public async void GivenAchivementJudgeWhenJudgeResultsThenResultAggregateIsUpdated()
        {
            //Arrange
            var resultAggregate = new ResultAggregate(){ExactHits = 7};
            var resultsRepo = new ResultRepositoryReturning(resultAggregate);
            var judge = new AchievementJudge(resultsRepo, Enumerable.Empty<AchievementLine>().ToArray());

            // Act
            var achivements = await judge.JudgeResult(new Results(){ ExtraHits = 2, TotalNotes = 10});

            // Assert
            Assert.That(resultAggregate.ExactHits, Is.EqualTo(15));
        }
    }

    public class ResultRepositoryReturning : IResultsRepository
    {
        private readonly ResultAggregate resultAggregate;

        public ResultRepositoryReturning(ResultAggregate resultAggregate)
        {
            this.resultAggregate = resultAggregate;
        }

        public void Add(Results results)
        {
            throw new System.NotImplementedException();
        }

        public Task<List<Results>> GetAllResults()
        {
            throw new System.NotImplementedException();
        }

        public Task<ResultAggregate> GetResultAggregate()
        {
           return new TaskFactory<ResultAggregate>().StartNew(task => resultAggregate, null);
        }

        public Task SaveResultAggregate(ResultAggregate aggregate)
        {
            return new TaskFactory().StartNew(() => { });
        }
    }

    public class AchievementLineReturning : AchievementLine
    {
        private readonly Achievement achievement;

        public AchievementLineReturning(Achievement achievement)
        {
            this.achievement = achievement;
        }

        public override string Message
        {
            get { return "Achievement!"; }
        }

        public override string Description
        {
            get { throw new System.NotImplementedException(); }
        }

        public override Achievement Judge(Results results, ResultAggregate aggregate)
        {
            return achievement;
        }

        public override StateBase CreateState()
        {
            throw new System.NotImplementedException();
        }
    }
}