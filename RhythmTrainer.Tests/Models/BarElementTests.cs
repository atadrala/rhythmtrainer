﻿using NUnit.Framework;
using RhythmTrainer.Models;

namespace RhythmTrainer.Tests.Models
{
    [TestFixture]
    public class BarElementTests
    {
        [Test]
        public void GivenQuarterNoteWhenConvertedToMilisecondsFor60BeatsPerMinThenShouldReturnOneSecond()
        {
            //Arrange
            var fullNote = new Quarter();

            //Act
            double miliseconds = fullNote.ToMiliseconds(4, 60);

            //Assert
            Assert.That(miliseconds, Is.EqualTo(1000).Within(0.1));
        }

        [Test]
        public void GivenFullNoteWhenConvertedToMilisecondsFor120BeatsPerMinThenShouldReturnTwoSeconds()
        {
            //Arrange
            var fullNote = new FullNote();

            //Act
            double miliseconds = fullNote.ToMiliseconds(4, 120);

            //Assert
            Assert.That(miliseconds, Is.EqualTo(2000).Within(0.1));
        }
    }
}