﻿using NUnit.Framework;
using RhythmTrainer.Models;
using RhythmTrainer.Service;

namespace RhythmTrainer.Tests.Models
{
    [TestFixture]
    public class DotedNoteTests
    {
         [Test]
         public void GivenBarScafoldForThreeFourthMeterWhenAddedTwoDotedQuartersThenBarIsComplete()
         {
             // Arrange
             var meter = new Meter(3, 4);
             var scafold = new BarScafold(meter);

             // Act
             scafold.AddNote(new DotedNote<Quarter>());
             scafold.AddNote(new DotedNote<Quarter>());

             // Assert
             Assert.That(scafold.IsComplete);
         }
    }
}