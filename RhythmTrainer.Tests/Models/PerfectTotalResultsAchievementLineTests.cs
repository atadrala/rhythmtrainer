﻿using NUnit.Framework;
using RhythmTrainer.Models;
using RhythmTrainer.Models.Achievements;

namespace RhythmTrainer.Tests.Models
{
    [TestFixture]
    public class PerfectTotalResultsAchievementLineTests
    {
        [Test]
        public void GivenFourPerfectResultsWhenTotalResultIsPerfectThenEarnedAchievementHasLevelOne()
        {
            // Arrange
            var sut = new PerfectTotalResultsAchievementLine();
            var result = new Results() { TotalNotes = 10, ExtraHits = 0, MissingNotes = 0, TotalError = 0, TotalDuration = 10 };
            var aggregate = new ResultAggregate() { PerfectResultCount = 4 };

            // Act
            var achievement = sut.Judge(result, aggregate);

            // Assert
            Assert.That(achievement, Is.Not.Null);
            Assert.That(achievement.Level, Is.EqualTo(1));
        }

        [Test]
        public void GivenFourPerfectResultsWhenTotalResultIsNotPerfectThenNoAchievement()
        {
            // Arrange
            var sut = new PerfectTotalResultsAchievementLine();
            var result = new Results() { TotalNotes = 10, ExtraHits = 0, MissingNotes = 2, TotalError = 0, TotalDuration = 10 };
            var aggregate = new ResultAggregate() { PerfectResultCount = 4 };

            // Act
            var achievement = sut.Judge(result, aggregate);

            // Assert
            Assert.That(achievement, Is.Null);
        }

        [Test]
        public void GivenBigNumberOfPerfectsWhenTotalResultIsPerfectThenNoAchievementEarned()
        {
            // Arrange
            var sut = new PerfectTotalResultsAchievementLine();
            var result = new Results() { TotalNotes = 10, ExtraHits = 0, MissingNotes = 0, TotalError = 0, TotalDuration = 10 };
            var aggregate = new ResultAggregate() { PerfectResultCount = 20000 };

            // Act
            var achievement = sut.Judge(result, aggregate);

            // Assert
            Assert.That(achievement, Is.Null);
        }
    }
}