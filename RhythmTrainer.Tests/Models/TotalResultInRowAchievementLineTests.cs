﻿using NUnit.Framework;
using RhythmTrainer.Models;
using RhythmTrainer.Models.Achievements;

namespace RhythmTrainer.Tests.Models
{
    [TestFixture]
    public class TotalResultInRowAchievementLineTests
    {
         [Test]
         public void GivenOneResultOverTresholdWhenTotalResultIsAboveTresholdThenAchievenentIsReturnWithLevelOne()
         {
             // Arrange
             var sut = new TotalResultInRowAchievementLine();
             var result = new Results() { TotalNotes = 10, ExtraHits = 0, MissingNotes = 0, TotalError = 0, TotalDuration = 10};
             var resultAggregate = new ResultAggregate();
             var state = resultAggregate.AchievementState(sut) as TotalResultInRowAchievementLine.State;
             
             // Act
             state.CurrentInARow = 1;
             var achievement = sut.Judge(result, resultAggregate);

             // Assert
             Assert.That(achievement, Is.Not.Null);
             Assert.That(achievement.Level, Is.EqualTo(1));
         }

         [Test]
         public void GivenFourResultOverTresholdWhenTotalResultIsBelowTresholdThenTotalResultsOverTresholdCountsFromZero()
         {
             // Arrange
             var sut = new TotalResultInRowAchievementLine();
             var result = new Results() { TotalNotes = 10, ExtraHits = 0, MissingNotes = 4, TotalError = 0, TotalDuration = 10 };
             var resultAggregate = new ResultAggregate();
             var state = resultAggregate.AchievementState(sut) as TotalResultInRowAchievementLine.State;

             // Act
             state.CurrentInARow = 4;
             state.Level = 1;
             var achievement1 = sut.Judge(result, resultAggregate);
             result.MissingNotes = 0;
             var achievement2 = sut.Judge(result, resultAggregate);
             var achievement3 = sut.Judge(result, resultAggregate);
             var achievement4 = sut.Judge(result, resultAggregate);
             var achievement5 = sut.Judge(result, resultAggregate);
             var achievement6 = sut.Judge(result, resultAggregate);

             // Assert
             Assert.That(achievement1, Is.Null);
             Assert.That(achievement2, Is.Null);
             Assert.That(achievement3, Is.Null);
             Assert.That(achievement4, Is.Null);
             Assert.That(achievement5, Is.Null);
             Assert.That(achievement6, Is.Not.Null);
         }
    }
}