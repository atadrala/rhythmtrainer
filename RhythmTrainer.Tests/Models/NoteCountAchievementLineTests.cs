﻿using NUnit.Framework;
using RhythmTrainer.Models;
using RhythmTrainer.Models.Achievements;

namespace RhythmTrainer.Tests.Models
{
    [TestFixture]
    public class NoteCountAchievementLineTests
    {
         [Test]
         public void GivenNineExactNoteHitsWhenResultsContainTwoExactHitsThenAchievementIsEarned()
         {
             // Arrange
             var sut = new NoteCountAchievementLine();
             var resultAggregate = new ResultAggregate() {ExactHits = 9};
             var results = new Results() {TotalNotes = 5, MissingNotes = 3};
             
             // Act
             var achievement = sut.Judge(results, resultAggregate);

             // Assert
             Assert.That(achievement, Is.Not.Null);
        }

        [Test]
        public void GivenNoAchieventYetWhenPassFirstTresholdThenEarnedAchieventHasLevelOne()
        {
            // Arrange
            var sut = new NoteCountAchievementLine();
            var resultAggregate = new ResultAggregate() { ExactHits = sut.Tresholds[0] - 1 };
            var results = new Results() { TotalNotes = 5, MissingNotes = 0 };

            // Act
            var achievement = sut.Judge(results, resultAggregate);

            // Assert
            Assert.That(achievement.Level, Is.EqualTo(1));  
        }

        [Test]
        public void GivenNoAchieventYetWhenPassSecondTresholdThenEarnedAchieventHasLevelTwo()
        {
            // Arrange
            var sut = new NoteCountAchievementLine();
            var resultAggregate = new ResultAggregate() { ExactHits = sut.Tresholds[1] - 10 };
            var results = new Results() { TotalNotes = 50, MissingNotes = 20 };

            // Act
            var achievement = sut.Judge(results, resultAggregate);

            // Assert
            Assert.That(achievement.Level, Is.EqualTo(2));
        }

        [Test]
        public void GivenAchievementOnLastLevelWhenGetLotOfNotesThenNoAchievementEarned()
        {
            // Arrange
            var sut = new NoteCountAchievementLine();
            var resultAggregate = new ResultAggregate() { ExactHits = 100000 };
            var results = new Results() { TotalNotes = 50 };

            // Act
            var achievement = sut.Judge(results, resultAggregate);

            // Assert
            Assert.That(achievement, Is.Null);
        }
    }
}