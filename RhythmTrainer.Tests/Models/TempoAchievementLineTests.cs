﻿using NUnit.Framework;
using RhythmTrainer.Models;
using RhythmTrainer.Models.Achievements;

namespace RhythmTrainer.Tests.Models
{
    [TestFixture]
    public class TempoAchievementLineTests
    {
        [Test]
        public void GivenNinePerfectResultsWhenPlayedPerfectResultInTempoThenAchievementIsEarned()
        {
            // Arrange
            var sut = new TempoAchievementLine();
            var result = new Results() { TotalNotes = 10, ExtraHits = 0, MissingNotes = 0, TotalError = 0, TotalDuration = 10, Tempo = 100 };
            var resultAggregate = new ResultAggregate();
            var state = resultAggregate.AchievementState(sut) as TempoAchievementLine.State;

            // Act
            state.TempoResults = 9;
            var achievement = sut.Judge(result, resultAggregate);

            // Assert
            Assert.That(achievement, Is.Not.Null);
            Assert.That(achievement.Level, Is.EqualTo(1));
        }

        [Test]
        public void GivenTempoAchievementsLineWhenLevelIncreaseThenRequiredIncrease()
        {
            // Arrange
            var sut = new TempoAchievementLine();
            var result = new Results() { TotalNotes = 10, ExtraHits = 0, MissingNotes = 0, TotalError = 0, TotalDuration = 10, Tempo = 70 };
            var resultAggregate = new ResultAggregate();
            var state = resultAggregate.AchievementState(sut) as TempoAchievementLine.State;

            // Act
            state.Level = 5;
            state.TempoResults = 1109;
            var achievement = sut.Judge(result, resultAggregate);
            result.Tempo = 150;
            var achievement2 = sut.Judge(result, resultAggregate);

            // Assert
            Assert.That(achievement, Is.Null);
            Assert.That(achievement2, Is.Not.Null);
        }

        [Test]
        public void GivenTempoAchievementsLineWhenResultsAreNotPerfectThenAchievementIsNotEarned()
        {
            // Arrange
            var sut = new TempoAchievementLine();
            var result = new Results() { TotalNotes = 10, ExtraHits = 0, MissingNotes = 2, TotalError = 0, TotalDuration = 10, Tempo = 70 };
            var resultAggregate = new ResultAggregate();
            var state = resultAggregate.AchievementState(sut) as TempoAchievementLine.State;

            // Act
            state.TempoResults = 4;
            var achievement = sut.Judge(result, resultAggregate);

            // Assert
            Assert.That(achievement, Is.Null);
        }
    }
}