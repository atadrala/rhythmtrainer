﻿using NUnit.Framework;
using RhythmTrainer.Models;
using RhythmTrainer.Service;

namespace RhythmTrainer.Tests.Models
{
    [TestFixture]
    public class DottedNoteTests
    {
         [Test]
         public void GivenBarScafoldForThreeFourthMeterWhenAddedTwoDottedQuartersThenBarIsComplete()
         {
             // Arrange
             var meter = new Meter(3, 4);
             var scafold = new BarScafold(meter);

             // Act
             scafold.AddNote(new DottedNote<Quarter>());
             scafold.AddNote(new DottedNote<Quarter>());

             // Assert
             Assert.That(scafold.IsComplete);
         }
    }
}