using System;
using RhythmTrainer.Models;
using Windows.UI.Xaml.Data;

namespace RhythmTrainer.Views.Controls
{
    public class MeterToWidthConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            double margin = 0d;
            var stringParameter = parameter as string;
            if (stringParameter != null)
            {
                double.TryParse(stringParameter, out margin);
            }

            var meter = (Meter)value;
            return margin + (double)360.0 * meter.Note / meter.Count;
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotImplementedException();
        }
    }
}