﻿using System;
using RhythmTrainer.Models;
using Windows.UI.Xaml.Data;

namespace RhythmTrainer.Views.Controls
{
    public class NoteHitToImageSourceConverter : IValueConverter 
    {     
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            var noteHit = (NoteHit.NoteHitType)value;
            
            return "/Assets/" + noteHit.ToString() + ".png";
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotImplementedException();
        }
    }
}
