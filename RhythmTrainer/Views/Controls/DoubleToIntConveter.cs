﻿using System;
using Windows.UI.Xaml.Data;

namespace RhythmTrainer.Views.Controls
{
    public class DoubleToIntConveter:IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            return (int) value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            return (double) value;
        }
    }
}