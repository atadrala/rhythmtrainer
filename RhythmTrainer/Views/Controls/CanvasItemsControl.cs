﻿using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Data;

namespace RhythmTrainer.Views.Controls
{
    public class CanvasItemsControl : ItemsControl 
    {
        protected override void PrepareContainerForItemOverride(DependencyObject element, object item)
        {
            var contentitem = element as FrameworkElement;
            var leftBinding = new Binding() { Path = new PropertyPath("Left") };
            contentitem.SetBinding(Canvas.LeftProperty, leftBinding);
            base.PrepareContainerForItemOverride(element, item);
        }
    }
}
