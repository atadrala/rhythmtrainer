﻿using RhythmTrainer.Models;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace RhythmTrainer.Views.Controls
{
    public class DotedNoteDataTemplateSelector : DataTemplateSelector
    {
        public DataTemplate FullNoteTemplate { get; set; }
        public DataTemplate HalfNoteTemplate { get; set; }
        public DataTemplate QuarterNoteTemplate { get; set; }
        public DataTemplate QuarterRestTemplate { get; set; }
        public DataTemplate HalfRestTemplate { get; set; }

        protected override DataTemplate SelectTemplateCore(object item, DependencyObject container)
        {
            var element = container as FrameworkElement;

            if (element != null && item != null && item is FullNote)
            {
                return FullNoteTemplate;
            }
            if (element != null && item != null && item is Half)
            {
                return HalfNoteTemplate;
            }
            if (element != null && item != null && item is Quarter)
            {
                return QuarterNoteTemplate;
            }
            if (element != null && item != null && item is QuarterRest)
            {
                return QuarterRestTemplate;
            }
            if (element != null && item != null && item is HalfRest)
            {
                return HalfRestTemplate;
            }
            return null;
        }
    }
}