﻿using System.Collections.Generic;
using System.Linq;
using Caliburn.Micro;
using PropertyChanged;
using RhythmTrainer.Models;
using Windows.UI.Xaml;

namespace RhythmTrainer.Views.Controls
{
    public sealed partial class MusicSheetView
    {
        public static readonly DependencyProperty MusicSheetProperty =
            DependencyProperty.Register("MusicSheet", typeof(MusicSheet), typeof(MusicSheetView),
            new PropertyMetadata(default(MusicSheet), MusicSheetChanged));

        public static readonly DependencyProperty CurrentBarNumberProperty =
            DependencyProperty.Register("CurrentBarNumber", typeof(int), typeof(MusicSheetView),
            new PropertyMetadata(-1, CurrentBarNumberChanged));

        public static readonly DependencyProperty IsMusicVisibleProperty =
            DependencyProperty.Register("IsMusicVisible", typeof (bool), typeof (MusicSheetView),
            new PropertyMetadata(true, MusicVisibilityChanged));

        public bool IsMusicVisible
        {
            get { return (bool) GetValue(IsMusicVisibleProperty); }
            set { SetValue(IsMusicVisibleProperty, value); }
        }

        private IList<BarViewModel> barItems;

        public MusicSheet MusicSheet
        {
            get { return (MusicSheet)GetValue(MusicSheetProperty); }
            set { SetValue(MusicSheetProperty, value); }
        }

        public int CurrentBarNumber
        {
            get { return (int)GetValue(CurrentBarNumberProperty); }
            set { SetValue(CurrentBarNumberProperty, value); }
        }

        public MusicSheetView()
        {
            InitializeComponent();
        }

        private static void MusicSheetChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var musicSheetView = d as MusicSheetView;
            if (musicSheetView != null)
            {
                musicSheetView.SetMusicSheet(e.NewValue as MusicSheet);
            }
        }

        private static void CurrentBarNumberChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var musicSheetView = d as MusicSheetView;
            if (musicSheetView != null)
            {
                musicSheetView.SetCurrentBarNumber((int)e.NewValue);
            }
        }

        private void SetCurrentBarNumber(int currentBarNumber)
        {
            if (barItems == null)
            {
                return;
            }
            foreach (var barViewModel in barItems)
            {
                barViewModel.IsSelected = false;
            }
            if (currentBarNumber >= 0 && currentBarNumber < barItems.Count)
            {
                barItems[currentBarNumber].IsSelected = true;
            }
        }

        public void SetMusicSheet(MusicSheet musicSheet)
        {
            barItems = musicSheet.Bars.Select(b => new BarViewModel(b, false, IsMusicVisible)).ToList();
            Bars.ItemsSource = barItems;
            Meter.Content = musicSheet.Meter;
        }

        private static void MusicVisibilityChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var musicSheetView = d as MusicSheetView;
            if (musicSheetView == null || musicSheetView.barItems == null)
            {
                return;
            }
            foreach (var barViewModel in musicSheetView.barItems)
            {
                barViewModel.IsVisible = (bool)e.NewValue;
            }
        }
    }

    [ImplementPropertyChanged]
    public class BarViewModel : PropertyChangedBase
    {
        public Bar Bar { get; set; }
        public bool IsSelected { get; set; }
        public bool IsVisible { get; set; }

        public BarViewModel(Bar bar, bool isSelected, bool isVisible)
        {
            Bar = bar;
            IsSelected = isSelected;
            IsVisible = isVisible;
        }
    }
}
