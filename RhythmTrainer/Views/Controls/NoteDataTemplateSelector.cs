﻿using System;
using RhythmTrainer.Models;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace RhythmTrainer.Views.Controls
{
    public class NoteDataTemplateSelector : DataTemplateSelector
    {
        public DataTemplate FullNoteTemplate { get; set; }
        public DataTemplate HalfNoteTemplate { get; set; }
        public DataTemplate QuarterNoteTemplate { get; set; }
        public DataTemplate EightNoteTemplate { get; set; }
        public DataTemplate NoteGroupTemplate { get; set; }
        public DataTemplate SixteenthGroupTemplate { get; set; }
        public DataTemplate QuarterRestTemplate { get; set; }
        public DataTemplate HalfRestTemplate { get; set; }
        public DataTemplate QuarterTrippleNoteGroupTemplate { get; set; }
        public DataTemplate DottedNoteTemplate { get; set; }

        protected override DataTemplate SelectTemplateCore(object item, DependencyObject container)
        {
            var element = container as FrameworkElement;

            if (element != null && item != null && item is FullNote)
            {
                return FullNoteTemplate;
            }
            if (element != null && item != null && item is Half)
            {
                return HalfNoteTemplate;
            }
            if (element != null && item != null && item is Quarter)
            {
                return QuarterNoteTemplate;
            }
            if (element != null && item != null && item is Eight)
            {
                return EightNoteTemplate;
            }
            if (element != null && item != null && (item is NoteGroup || item is TrippleNoteGroup))
            {
                return NoteGroupTemplate;
            }
            if (element != null && item != null && item is SixteenGroup)
            {
                return SixteenthGroupTemplate;
            }
            if (element != null && item != null && item is QuarterRest)
            {
                return QuarterRestTemplate;
            }
            if (element != null && item != null && item is HalfRest)
            {
                return HalfRestTemplate;
            }
            if (element != null && item != null && (item is QuarterTrippleNoteGroup))
            {
                return QuarterTrippleNoteGroupTemplate;
            }
            if (element != null && item != null)
            {
                if (item.GetType().IsConstructedGenericType &&
                    item.GetType().GetGenericTypeDefinition() == typeof (DottedNote<>))
                {
                    return DottedNoteTemplate;
                }
            }
            return null;
        }
    }
}
