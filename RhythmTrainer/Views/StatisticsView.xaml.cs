﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using WinRTXamlToolkit.Controls.DataVisualization.Charting;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Text;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace RhythmTrainer.Views
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class StatisticsView : Page
    {
        public StatisticsView()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.  The Parameter
        /// property is typically used to configure the page.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            ((LineSeries)DailyProgressChart.Series[0]).DependentRangeAxis = new LinearAxis
                {
                    Minimum = 0,
                    Maximum = 100,
                    Orientation = AxisOrientation.Y,
                    Interval = 20,
                    ShowGridLines = true,
                    FontSize = 20,
                };

            ((LineSeries)DailyProgressChart.Series[0]).IndependentAxis = new DateTimeAxis()
                {
                    Orientation = AxisOrientation.X,
                    FontSize = 20,
                };

            ((ColumnSeries)LevelAverageScoreChart.Series[0]).DependentRangeAxis = new LinearAxis
                 {
                     Minimum = 0,
                     Maximum = 100,
                     Orientation = AxisOrientation.Y,
                     Interval = 20,
                     ShowGridLines = true,
                     FontSize = 20,
                 };

            ((ColumnSeries)LevelAverageScoreChart.Series[0]).IndependentAxis = new CategoryAxis
                {
                    Orientation = AxisOrientation.X,
                    FontSize = 22,
                };

            DailyProgressChart.LegendItems.Clear();
            LevelAverageScoreChart.LegendItems.Clear();
        }
    }
}
