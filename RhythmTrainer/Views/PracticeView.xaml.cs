﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Animation;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace RhythmTrainer.Views
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class PracticeView : Page
    {
        public PracticeView()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.  The Parameter
        /// property is typically used to configure the page.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
        }

        private void BpmOnClick(object sender, RoutedEventArgs e)
        {
            DisplayPopup(BpmPopup);
        }

        private void BarNumberOnClick(object sender, RoutedEventArgs e)
        {
            DisplayPopup(BarNumberPopup);
        }

        private void DisplayPopup(Popup popup)
        {
            var inAnimation = new Storyboard();
            var popin = new PopInThemeAnimation();

            UpdateLayout();

            popin.FromVerticalOffset = -8;
            popin.FromHorizontalOffset = 0;

            Storyboard.SetTarget(popin, popup);
            inAnimation.Children.Add(popin);
            inAnimation.Begin();

            popup.IsOpen = true;
        }
    }
}
