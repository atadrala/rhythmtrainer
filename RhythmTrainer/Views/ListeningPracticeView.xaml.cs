﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Animation;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace RhythmTrainer.Views
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class ListeningPracticeView : Page
    {
        public ListeningPracticeView()
        {
            this.InitializeComponent();
        }

        private void BpmOnClick(object sender, RoutedEventArgs e)
        {
            DisplayPopup(BpmPopup);
        }

        private void BarNumberOnClick(object sender, RoutedEventArgs e)
        {
            DisplayPopup(BarNumberPopup);
        }

        private void DisplayPopup(Popup popup)
        {
            var inAnimation = new Storyboard();
            var popin = new PopInThemeAnimation();

            UpdateLayout();

            popin.FromVerticalOffset = -8;
            popin.FromHorizontalOffset = 0;

            Storyboard.SetTarget(popin, popup);
            inAnimation.Children.Add(popin);
            inAnimation.Begin();

            popup.IsOpen = true;
        }
    }
}
