﻿namespace RhythmTrainer.Models
{
    public class Eight : BarElement
    {
        public Eight()
        {
            Width = 45;
        }

        public override Rational NoteFraction { get { return  new Rational(1, 8); } }
    }
}