﻿using System.Collections.Generic;

namespace RhythmTrainer.Models
{
    public abstract class BarElement
    {
        protected BarElement()
        {
            Width = 60;
        }

        public double Width { get; set; }

        public abstract Rational NoteFraction { get; }

        public virtual IEnumerable<BarElement> Notes
        {
            get { return new[] {this}; }
        }

        public virtual double ToMiliseconds(int meterCount, double beatsPerMinute)
        {
            return (60000d*meterCount*(double) NoteFraction)/(beatsPerMinute);
        }
    }
}
