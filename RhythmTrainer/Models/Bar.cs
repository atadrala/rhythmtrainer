﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Caliburn.Micro;

namespace RhythmTrainer.Models
{
    public class Bar
    {
        public Meter Meter { get; private set; }
        public BindableCollection<BarElement> Notes { get; private set; }
        public BindableCollection<Tick> Ticks { get; private set; }
        public BindableCollection<NoteHit> NoteHits { get; private set; }

        public IEnumerable<BarElement> AllNotes
        {
            get { return Notes.SelectMany(n => n.Notes); }
        }

        public Bar(IEnumerable<BarElement> notes, Meter meter)
        {
            Notes = new BindableCollection<BarElement>(notes);
            Ticks = new BindableCollection<Tick>();
            NoteHits = new BindableCollection<NoteHit>();
            Meter = meter;
        }
    }
}