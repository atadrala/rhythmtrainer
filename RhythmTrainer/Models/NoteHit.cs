﻿namespace RhythmTrainer.Models
{
    public class NoteHit
    {
        public double Left { get; set; }
        public NoteHitType Hit { get; set; }

        public enum NoteHitType
        {
            None = 0,
            Missing = 1,
            Early = 2,
            Late = 3,
            Exact = 64,
        }
    }
}