﻿using System.Linq;

namespace RhythmTrainer.Models
{
    public class QuarterRest : Rest
    {
        public override Rational NoteFraction
        {
            get { return new Rational(1,4); }
        }
    }

    public class HalfRest : Rest
    {
        public HalfRest()
        {
            Width = 120;
        }

        public override Rational NoteFraction
        {
            get { return new Rational(1, 2); }
        }
    }

    public abstract class Rest : BarElement
    {
    }
}