﻿namespace RhythmTrainer.Models
{
    public class Meter 
    {
        public int Count { get; set; }

        public int Note { get; set; }

        public Meter()
        {
        }

        public Meter(int note, int count)
        {
            Note = note;
            Count = count;
        }
    }
}