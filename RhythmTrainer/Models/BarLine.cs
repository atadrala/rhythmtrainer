﻿namespace RhythmTrainer.Models
{
    class BarLine
    {
        public BarLine()
        {
            Width = 10;
        }

        protected int Width{ get; set; }
    }
}