﻿using System;
using System.Collections.Generic;
using RhythmTrainer.Service;

namespace RhythmTrainer.Models
{
    public class ResultAggregate
    {
        public ResultAggregate()
        {
            AchievementStates = new Dictionary<string, AchievementLine.StateBase>();
        }

        public int PlayedNotes { get; set; } 

        public int PlayedExcercises { get; set; } 

        public int ExactHits { get; set; }

        public int PerfectResultCount { get; set; }

        public IDictionary<string, AchievementLine.StateBase> AchievementStates { get; set; }

        public void Update(Results results)
        {
            ExactHits += results.TotalNotes - results.ExtraHits - results.MissingNotes;
            PlayedNotes += results.TotalNotes - results.MissingNotes + results.ExtraHits;
            PlayedExcercises++;
        }

        public AchievementLine.StateBase AchievementState(AchievementLine achievementLine)
        {
            AchievementLine.StateBase result = null;
            if (AchievementStates.ContainsKey(achievementLine.GetType().FullName) == false)
            {
                AchievementStates[achievementLine.GetType().FullName] = achievementLine.CreateState();
            }
            return AchievementStates[achievementLine.GetType().FullName];
        }
    }
}