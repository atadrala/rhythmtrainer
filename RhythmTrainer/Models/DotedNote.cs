﻿namespace RhythmTrainer.Models
{
    public class DotedNote<TNote> : BarElement where TNote : BarElement, new()
    {
        private readonly TNote _note;
        private readonly Rational _dotedFraction = new Rational(3, 2);

        public TNote Note
        {
            get { return _note; }
        }
 
        public DotedNote()
        {
            _note = new TNote();
            Width = _note.Width*1.5;
        }

        public override Rational NoteFraction
        {
            get
            {
                return _dotedFraction * _note.NoteFraction;
            }
        }
    }
}