﻿namespace RhythmTrainer.Models
{
    public class Achievement
    {
        private readonly string achievementMessage;
        private readonly int level;

        public Achievement(string achievementMessage, int level)
        {
            this.achievementMessage = achievementMessage;
            this.level = level;
        }

        public int Level
        {
            get { return level; }
        }

        public override string ToString()
        {
            return achievementMessage + " Level " + level;
        }
    }
}