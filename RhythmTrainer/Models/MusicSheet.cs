﻿using System;
using System.Collections.Generic;

namespace RhythmTrainer.Models
{
    public class MusicSheet
    {
        public Meter Meter { get; set; }
        public IEnumerable<Bar> Bars { get; set; }

        public TimeSpan GetBarDuration(double bpm)
        {
            return TimeSpan.FromMilliseconds(Meter.Note * 60000.0d / bpm);
        }
    }
}
