﻿namespace RhythmTrainer.Models
{
    public class DottedNote<TNote> : BarElement where TNote : BarElement, new()
    {
        private readonly TNote _note;
        private readonly Rational _dottedFraction = new Rational(3, 2);

        public TNote Note
        {
            get { return _note; }
        }
 
        public DottedNote()
        {
            _note = new TNote();
            Width = _note.Width*1.5;
        }

        public override Rational NoteFraction
        {
            get
            {
                return _dottedFraction * _note.NoteFraction;
            }
        }
    }
}