﻿namespace RhythmTrainer.Models
{
    public class Half : BarElement
    {
        public Half()
        {
            Width = 180;
        }

        public override Rational NoteFraction { get { return new Rational(1, 2); } }
    }
}