﻿namespace RhythmTrainer.Models
{
    public class Rational
    {
        public int Numerator  { get; private set; }

        public int Denominator { get; private set; }

        public Rational(int numerator, int denominator)
        {
            var gcd = Gcd(numerator, denominator);
            Numerator = numerator / gcd;
            Denominator = denominator / gcd;
        }

        private int Gcd(int a, int b)
        {
            if (a == 0)
            {
                return b;
            }
            return Gcd(b % a, a);
        }

        public static Rational operator +(Rational a, Rational b)
        {
            return new Rational(a.Numerator*b.Denominator + b.Numerator*a.Denominator, a.Denominator*b.Denominator);
        }

        public static Rational operator *(Rational a, Rational b)
        {
            return new Rational(a.Numerator *  b.Numerator, a.Denominator * b.Denominator);
        }

        public static Rational operator *(int a, Rational b)
        {
            return new Rational(a*b.Numerator, b.Denominator);
        }

        public static bool operator ==(int a, Rational b)
        {
            return b.Denominator == 1 && b.Numerator == a;
        }

        public static bool operator !=(int a, Rational b)
        {
            return !(a == b);
        }

        public static Rational operator -(Rational a, Rational b)
        {
            return new Rational(a.Numerator * b.Denominator - b.Numerator * a.Denominator, a.Denominator * b.Denominator);
        }

        public static explicit operator double(Rational a)
        {
            return ((double) a.Numerator)/a.Denominator;
        }

        public static bool operator <=(Rational a, Rational b)
        {
            return a.Numerator*b.Denominator <= b.Numerator*a.Denominator;
        }

        public static bool operator >=(Rational a, Rational b)
        {
            throw new System.NotImplementedException();
        }

        protected bool Equals(Rational other)
        {
            return Numerator == other.Numerator && Denominator == other.Denominator;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((Rational)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (Numerator * 397) ^ Denominator;
            }
        }

    }
}