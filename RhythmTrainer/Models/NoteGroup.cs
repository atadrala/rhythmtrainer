﻿using System.Collections.Generic;

namespace RhythmTrainer.Models
{
    public class NoteGroup : BarElement
    {
        private readonly IEnumerable<NoteGroupElement> _notes;

        public NoteGroup()
        {
            Width = 90;
            _notes = new[]
                {
                    new NoteGroupElement(new Rational(1,8)) {Width = 45, IsNotLast = true},
                    new NoteGroupElement(new Rational(1,8)) {Width = 45, IsNotFirst = true},
                };
        }

        public override Rational NoteFraction
        {
            get { return new Rational(1, 4); }
        }

        public override IEnumerable<BarElement> Notes
        {
            get { return _notes; }
        }
    }
}