﻿using System.Collections.Generic;

namespace RhythmTrainer.Models
{
    public class SixteenGroup : BarElement
    {
        private readonly IEnumerable<NoteGroupElement> _notes;

        public SixteenGroup()
        {
            Width = 90;
            _notes = new[]
            {
                new NoteGroupElement(new Rational(1,16)) {Width = 22, IsNotLast = true},
                new NoteGroupElement(new Rational(1,16)) {Width = 22, IsNotFirst = true, IsNotLast = true},
                new NoteGroupElement(new Rational(1,16)) {Width = 22, IsNotFirst = true, IsNotLast = true},
                new NoteGroupElement(new Rational(1,16)) {Width = 22, IsNotFirst = true},
            };
        }

        public override Rational NoteFraction
        {
            get { return new Rational(1, 4); }
        }

        public override IEnumerable<BarElement> Notes
        {
            get { return _notes; }
        } 
    }
}