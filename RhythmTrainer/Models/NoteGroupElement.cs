﻿namespace RhythmTrainer.Models
{
    public class NoteGroupElement : BarElement
    {
        private Rational _noteFraction;

        public NoteGroupElement(Rational noteFraction)
        {
            _noteFraction = noteFraction;
        }

        public override Rational NoteFraction
        {
            get { return _noteFraction; }
        }

        public bool IsNotLast { get; set; }
        public bool IsNotFirst { get; set; }
    }
}