﻿using System.Collections.Generic;

namespace RhythmTrainer.Models
{
    public class  QuarterTrippleNoteGroup : BarElement
    {
        private readonly IEnumerable<NoteGroupElement> _notes;

        public QuarterTrippleNoteGroup()
        {
            Width = 60;
            _notes = new[]
                {
                    new NoteGroupElement(new Rational(1,6)) {Width = 20, IsNotLast = true},
                    new NoteGroupElement(new Rational(1,6)) {Width = 20, IsNotFirst = true, IsNotLast = true},
                    new NoteGroupElement(new Rational(1,6)) {Width = 20, IsNotFirst = true},
                };
        }

        public override Rational NoteFraction
        {
            get { return new Rational(1,2); }
        }

        public override IEnumerable<BarElement> Notes
        {
            get { return _notes; }
        } 
    }
}