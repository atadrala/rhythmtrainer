﻿namespace RhythmTrainer.Models
{
    public class Quarter : BarElement
    {
        public Quarter()
        {
            Width = 90;
        }

        public override Rational NoteFraction { get { return new Rational(1, 4); } }
    }
}