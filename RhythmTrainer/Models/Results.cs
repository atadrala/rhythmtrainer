﻿using System;

namespace RhythmTrainer.Models
{
    public class Results
    {
        public string LevelTitle { get; set; }

        public DateTime DateTime { get; set; }

        public double TotalDuration { get; set; }

        public double TotalError { get; set; }

        public double TotalResult {get
        {
            return (double) (TotalNotes - MissingNotes)/TotalNotes - TotalError/TotalDuration -
                   (double) ExtraHits/TotalNotes;
        }}

        public int MissingNotes { get; set; }

        public int ExtraHits { get; set; }

        public int TotalNotes { get; set; }

        public double Tempo { get; set; }

        public string TotalErrorString { get { return string.Format("{0:P}", TotalError/TotalDuration); } }

        public string AccuracyString
        {
            get
            {
                return string.Format("{0}/{1} ({2:P})",
                                     TotalNotes - MissingNotes,
                                     TotalNotes,
                                     (double) (TotalNotes - MissingNotes)/TotalNotes);
            }
        }

        public string ExtraHitsString
        {
            get
            {
                return string.Format("{0}/{1} ({2:P})", 
                                     ExtraHits, 
                                     TotalNotes, 
                                     (double) ExtraHits / TotalNotes);
            }
        }

        public string TotalResultString
        {
            get { return string.Format("{0:P}", TotalResult); }
        }

        public string Summary
        {
            get
            {
                var value = TotalResult;

                if (value > 0.8) return "Excellent!";
                if (value > 0.6) return "Good!";
                if (value > 0.4) return "Don't give up!";
                return "Maybe next time...";
            }
        }
    }
}