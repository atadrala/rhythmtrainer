﻿using RhythmTrainer.Service;

namespace RhythmTrainer.Models.Achievements
{
    public class TotalResultInRowAchievementLine : AchievementLine
    {
        private readonly int inRowCount = 0;
        private readonly int scoreTreshold = 1;
        private readonly int[,] Tresholds = new [,]
            {
                {2,75},
                {5,75},
                {10,75},
                {15,75},
                {2,90},
                {5,90},
                {10,90},
                {15,90},
                {2,99},
                {5,99},
                {10,99},
                {15,99},
            };

        public override string Message
        {
            get { return "Excellent in a row!"; }
        }

        public override string Description
        {
            get { return "Achievement is earned when Total Score above treshold is achievent certain number of times in a row."; }
        }

        public override Achievement Judge(Results results, ResultAggregate aggregate)
        {
            var state = aggregate.AchievementState(this) as State;
            if (Tresholds[state.Level, scoreTreshold] <= 100*results.TotalResult &&
                state.CurrentInARow + 1 == Tresholds[state.Level, inRowCount])
            {
                state.Level++;
                state.CurrentInARow = 0;
                return new Achievement(Message, state.Level);
            }

            if (Tresholds[state.Level, 1] <= 100*results.TotalResult)
            {
                state.CurrentInARow ++;
            }
            else
            {
                state.CurrentInARow = 0;
            }
            return null;
        }

        public override StateBase CreateState()
        {
            return new State();
        }

        public class State : StateBase
        {
            public int CurrentInARow { get; set; }
        }
    }
}