﻿using System.Collections.Generic;
using RhythmTrainer.Service;

namespace RhythmTrainer.Models.Achievements
{
    public class PerfectTotalResultsAchievementLine : AchievementLine
    {
        public readonly List<int> Tresholds = new List<int> { 5, 50, 250, 1000, 2500, 5000, 10000 };

        public override string Message
        {
            get { return "Perfect result!"; }
        }

        public override string Description
        {
            get { return "Achievement is earned when 100% Total Score was achieved certain number of times while practicing."; }
        }

        public override Achievement Judge(Results results, ResultAggregate aggregate)
        {
            if (results.TotalResult > .99)
            {
                aggregate.PerfectResultCount++;
                if (Tresholds.Contains(aggregate.PerfectResultCount))
                {
                    var level = Tresholds.IndexOf(aggregate.PerfectResultCount) + 1;
                    aggregate.AchievementState(this).Level = level;
                    return new Achievement(Message, level);
                }
            }
            return null;
        }
    }
}