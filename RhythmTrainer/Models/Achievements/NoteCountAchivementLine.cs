﻿using System.Linq;
using RhythmTrainer.Service;

namespace RhythmTrainer.Models.Achievements
{
    public class NoteCountAchievementLine : AchievementLine
    {
        public readonly int[] Tresholds = new int[] { 10, 100, 500, 2000, 5000, 10000, 18000, 30000, 45000, 50000 };

        public override string Message
        {
            get { return "Note number!"; }
        }

        public override string Description
        {
            get { return string.Format("Acheivement is earned by playing certain number of exact note hits."); }
        }

        public override Achievement Judge(Results results, ResultAggregate aggregate)
        {
            var exactHits = results.TotalNotes - results.MissingNotes;
            if (exactHits + aggregate.ExactHits > GetHitTreshold(aggregate.ExactHits))
            {
                var level = GetAchivementLevel(aggregate.ExactHits);
                aggregate.AchievementState(this).Level = level;
                return new Achievement(Message, level);
            }
            return null;
        }

        private int GetAchivementLevel(int exactHits)
        {
           return Tresholds.Count(x => exactHits >= x) + 1;
        }

        private int GetHitTreshold(int exactHits)
        {
            var result = Tresholds.FirstOrDefault(x => exactHits < x);
            return result == 0 ? int.MaxValue : result;
        }
    }
}