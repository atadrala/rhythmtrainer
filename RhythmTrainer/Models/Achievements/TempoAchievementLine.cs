﻿using RhythmTrainer.Service;

namespace RhythmTrainer.Models.Achievements
{
    public class TempoAchievementLine : AchievementLine
    {
        private int[] temposTreshold = new[] {90, 90, 90, 120, 120, 120, 150, 150, 150, 180, 180, 180};
        private int[] resultsCountThreshold = new int[]{10, 100, 500, 510, 610, 1110, 1120, 12200, 2000, };

        public override string Message
        {
            get { return "Greate tempo!"; }
        }

        public override string Description
        {
            get { return "Achievement is earned when 100% Total score was achieved with certain tempo."; }
        }

        public override StateBase CreateState()
        {
            return  new State();
        }

        public override Achievement Judge(Results results, ResultAggregate aggregate)
        {
            var state = aggregate.AchievementState(this) as State;

            if (results.Tempo > temposTreshold[state.Level] && results.TotalResult > 0.99)
            {
                state.TempoResults++;

                if (state.TempoResults == resultsCountThreshold[state.Level])
                {
                    state.Level++;
                    return new Achievement(Message, state.Level);
                }
            }
            return null;
        }

        public class State : StateBase
        {
            public int TempoResults { get; set; }
        }
    }
}