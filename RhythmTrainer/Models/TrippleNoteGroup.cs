﻿using System.Collections.Generic;

namespace RhythmTrainer.Models
{
    public class TrippleNoteGroup : BarElement
    {
        private readonly IEnumerable<NoteGroupElement> _notes;

        public TrippleNoteGroup()
        {
            Width = 90;
            _notes = new[]
                {
                    new NoteGroupElement( new Rational(1, 12)) {Width = 30, IsNotLast = true},
                    new NoteGroupElement( new Rational(1, 12)) {Width = 30, IsNotFirst = true, IsNotLast = true},
                    new NoteGroupElement( new Rational(1, 12)) {Width = 30, IsNotFirst = true},
                };
        }

        public override Rational NoteFraction
        {
            get { return new Rational(1, 4); }
        }

        public override IEnumerable<BarElement> Notes
        {
            get { return _notes; }
        } 
    }
}