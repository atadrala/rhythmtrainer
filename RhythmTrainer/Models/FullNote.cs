﻿namespace RhythmTrainer.Models
{
    public class FullNote : BarElement
    {
        public FullNote()
        {
            Width = 360;
        }

        public override Rational NoteFraction { get { return new Rational(1, 1); } }
    }
}