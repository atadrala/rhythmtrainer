﻿using System;
using System.Linq;
using RhythmTrainer.Models;

namespace RhythmTrainer.Service
{
    public class Judge
    {
        public Results JudgeRecording(Recorder recorder, Metronome metronome, MusicSheet musicSheet)
        {
            var state = new JudgeState(recorder);
            foreach (var bar in musicSheet.Bars)
            {
                bar.NoteHits.Clear();
                state.ResetNoteOffset();

                foreach (var note in bar.AllNotes)
                {
                    if (state.CheckIfIsRest(note) == false)
                    {
                        state.MoveToNextClosestRecordedHit(note);

                        if (state.CheckIfFound(bar))
                        {
                            state.EstimateTimeDelay(bar);
                        }
                    }

                    state.UpdateCurentNoteState(note, musicSheet, metronome);
                }
            }

            state.CountLastExtraHits();

            var totalNotes = musicSheet.Bars.SelectMany(x => x.Notes).Count();

            return new Results
                {
                    DateTime = DateTime.Now,
                    TotalDuration = musicSheet.Bars.Count() * musicSheet.GetBarDuration(metronome.BeatsPerMinute).TotalMilliseconds,
                    TotalNotes = totalNotes,
                    TotalError = state.TotalError,
                    MissingNotes = state.MissingNotes,
                    ExtraHits = state.ExtraHits,
                    Tempo = metronome.BeatsPerMinute,
                };
        }
    }
}
