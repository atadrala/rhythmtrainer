﻿using RhythmTrainer.Models;

namespace RhythmTrainer.Service.Generators
{
    public class NoteGroupGenerator<T> : TimeRestrictedNoteGenerator<T> where T : BarElement, new()
    {
        public NoteGroupGenerator(Rational noteFraction)
            : base( noteFraction)
        {
        }

        public override bool CanGenerateFor(BarScafold barScafold)
        {
            if (( 4* barScafold.MeterLeft).Denominator != 1) return false;
            return base.CanGenerateFor(barScafold);
        }
    }
}