﻿using RhythmTrainer.Models;

namespace RhythmTrainer.Service.Generators
{
    public interface INoteGenerator
    {
        BarElement GenerateNotes();
        Rational NoteMeter { get; }
        bool CanGenerateFor(BarScafold barScafold);
    }
}