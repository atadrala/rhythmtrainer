﻿using System;
using RhythmTrainer.Models;

namespace RhythmTrainer.Service.Generators
{
    public class TimeRestrictedNoteGenerator<T> : INoteGenerator where T : BarElement, new()
    {
        private readonly Rational _noteFraction;

        public TimeRestrictedNoteGenerator(Rational noteFraction)
        {
            _noteFraction = noteFraction;
        }

        public BarElement GenerateNotes()
        {
            return new T();
        }

        public Rational NoteMeter
        {
            get { return _noteFraction; }
        }

        public virtual bool CanGenerateFor(BarScafold barScafold)
        {
            return NoteMeter <= barScafold.MeterLeft;
        }
    }
}