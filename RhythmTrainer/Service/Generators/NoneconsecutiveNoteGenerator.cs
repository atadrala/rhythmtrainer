﻿using System.Linq;
using RhythmTrainer.Models;

namespace RhythmTrainer.Service.Generators
{
    public class NoneconsecutiveNoteGenerator<T> : TimeRestrictedNoteGenerator<T> where T : BarElement, new()
    {
        public NoneconsecutiveNoteGenerator(Rational noteFraction)
            : base(noteFraction)
        {
        }

        public override bool CanGenerateFor(BarScafold barScafold)
        {
            var canGenerate = base.CanGenerateFor(barScafold);
            if (canGenerate)
            {
                if ((2 * NoteMeter).Equals(barScafold.MeterLeft))
                {
                    return false;
                }

                var note = barScafold.Notes.LastOrDefault();
                return note == null || (note is T) == false;
            }
            return false;
        }
    }
}