﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RhythmTrainer.Models;

namespace RhythmTrainer.Service
{
    public class GeneratorSchema
    {
        public static GeneratorSchema Quarters { get; private set; }

        public static GeneratorSchema EightsGroup { get; private set; }

        public static GeneratorSchema Eights { get; private set; }

        public static GeneratorSchema SixteenthGroup { get; private set; }

        public static GeneratorSchema Halves { get; private set; }

        public static GeneratorSchema Triples { get; private set; }

        public static GeneratorSchema Pauses { get; private set; }

        public static GeneratorSchema QuarterTriples { get; private set; }

        public static GeneratorSchema TripleMeter { get; private set; }

        public static GeneratorSchema MarchMeter { get; private set; }

        public static GeneratorSchema AdvancedMeter { get; set; }

        public static GeneratorSchema FiveFourthMeter { get; private set; }

        public static GeneratorSchema DottedNotes { get; private set; }

        protected IDictionary<Type, double> Probabilities { get; set; }

        public Meter Meter { get; set; }


        static GeneratorSchema()
        {
            Halves = new GeneratorSchema(new Meter(4, 4), new Dictionary<Type, double>
                {
                    {typeof (Half), 0.7d},
                    {typeof (FullNote), 0.3d}
                });

            Quarters = new GeneratorSchema(new Meter(4, 4), new Dictionary<Type, double>
                {
                    {typeof (FullNote), 0.2d},
                    {typeof (Half), 0.35d},
                    {typeof (Quarter), 0.45d},
                });

            Eights = new GeneratorSchema(new Meter(4, 4), new Dictionary<Type, double>
                {
                    {typeof (FullNote), 0.15d},
                    {typeof (Half), 0.15d},
                    {typeof (Quarter), 0.25d},
                    {typeof (Eight), 0.25d},
                    {typeof (NoteGroup), 0.20d},
                });

            EightsGroup = new GeneratorSchema(new Meter(4, 4), new Dictionary<Type, double>
                {
                    {typeof (FullNote), 0.15d},
                    {typeof (Half), 0.15d},
                    {typeof (Quarter), 0.35d},
                    {typeof (NoteGroup), 0.35d},
                });

            SixteenthGroup = new GeneratorSchema(new Meter(4, 4), new Dictionary<Type, double>
                {
                    {typeof (Half), 0.35d},
                    {typeof (Quarter), 0.45d},
                    {typeof (SixteenGroup), .20d},
                });

            Triples = new GeneratorSchema(new Meter(4, 4), new Dictionary<Type, double>
                {
                    {typeof(Half), 0.2d},
                    {typeof (Quarter), 0.4d},
                    {typeof (TrippleNoteGroup), 0.4d},
                });

            QuarterTriples = new GeneratorSchema(new Meter(4, 4), new Dictionary<Type, double>
                {
                    {typeof(Half), 0.2d},
                    {typeof (Quarter), 0.4d},
                    {typeof (TrippleNoteGroup), 0.4d},
                });

            Pauses = new GeneratorSchema(new Meter(4, 4), new Dictionary<Type, double>
                {
                    {typeof (Half), 0.15d},
                    {typeof (Quarter), 0.15d},
                    {typeof (QuarterTrippleNoteGroup), 0.15d},
                    {typeof (QuarterRest), 0.25d},
                    {typeof (HalfRest), 0.3d},
                });

            DottedNotes = new GeneratorSchema(new Meter(4, 4), new Dictionary<Type, double>
                {
                    {typeof (Half), 0.2d},
                    {typeof (Quarter), 0.2d},
                    {typeof (DottedNote<Quarter>), 0.15d},
                    {typeof (DottedNote<Half>), 0.15d},
                    {typeof (QuarterRest), 0.15d},
                    {typeof (Eight), 0.15d},
                });


            TripleMeter = new GeneratorSchema(new Meter(3, 4), new Dictionary<Type, double>
                {
                    {typeof (FullNote), 0.1d},
                    {typeof (Half), 0.15d},
                    {typeof (Quarter), 0.25d},
                    {typeof (Eight), 0.25d},
                    {typeof (QuarterRest), 0.15d},
                    {typeof (HalfRest), 0.1d},
                });

            MarchMeter = new GeneratorSchema(new Meter(2, 4), new Dictionary<Type, double>
                {
                    {typeof (Half), 0.15d},
                    {typeof (Quarter), 0.35d},
                    {typeof (Eight), 0.35d},
                    {typeof (QuarterRest), 0.15d},
                    {typeof (HalfRest), 0.1d},
                });

            FiveFourthMeter = new GeneratorSchema(new Meter(5, 4), new Dictionary<Type, double>
                {
                    {typeof (FullNote), 0.1d},
                    {typeof (Half), 0.15d},
                    {typeof (Quarter), 0.25d},
                    {typeof (Eight), 0.35d},
                    {typeof (QuarterRest), 0.15d},
                    {typeof (HalfRest), 0.1d},
                });

            AdvancedMeter = new GeneratorSchema(new Meter(7, 8), new Dictionary<Type, double>
                {
                    {typeof (Half), 0.15d},
                    {typeof (Quarter), 0.35d},
                    {typeof (Eight), 0.35d},
                    {typeof (QuarterRest), 0.15d},
                    {typeof (HalfRest), 0.1d},
                });

        }

        private GeneratorSchema(Meter meter, Dictionary<Type, double> probabilities)
        {
            Probabilities = probabilities;
            Meter = meter;
        }

        public double Probability<T>()
        {
            return Probabilities.ContainsKey(typeof(T)) ? Probabilities[typeof(T)] : 0.0d;
        }
    }
}
