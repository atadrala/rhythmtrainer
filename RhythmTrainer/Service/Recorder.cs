﻿using System;
using System.Collections.Generic;
using System.Linq;
using RhythmTrainer.Models;

namespace RhythmTrainer.Service
{
    public class Recorder
    {
        private int _currentMetronomeBar;

        public IList<RecordEvent> RecordEvents { get; private set; }

        public Recorder()
        {
            RecordEvents = new List<RecordEvent>();
        }

        public void RecordTap()
        {
            RecordTap(DateTime.Now);
        }

        public void RecordTap(DateTime time)
        {
            RecordEvents.Add(new RecordEvent {Time = time, Bar = _currentMetronomeBar, IsMetronomeTick = false});      
        }

        public void RecordTick(MetronomeTick tick)
        {
            RecordTick(tick, DateTime.Now);
        }

        public void RecordTick(MetronomeTick tick, DateTime time)
        {
            _currentMetronomeBar = tick.Bar;
            RecordEvents.Add(new RecordEvent {Time = time, Bar = tick.Bar, Tick = tick.Tick,
                IsMetronomeTick = true, StartsFirstMusicBar = tick.MusicBar == 0 && tick.Tick == 0 });
        }

        public void ProcessRecording()
        {
            int currentBar = 0;
            TimeSpan delta = TimeSpan.FromMilliseconds(120);

            foreach (var recordEvent in RecordEvents.OrderBy(r => r.IsMetronomeTick ? r.Time.Subtract(delta) : r.Time))
            {
                if (recordEvent.IsMetronomeTick)
                {
                    currentBar = recordEvent.Bar;
                }
                else
                {
                    recordEvent.Bar = currentBar;
                }
            }

            RecordEvent previous = null;
            foreach (var recordEvent in RecordEvents)
            {
                recordEvent.Delay = recordEvent.Time - (previous ?? recordEvent).Time;
                previous = recordEvent;
            }
        }

        public IEnumerable<RecordEvent> GetTapsFor(int barIndex)
        {
            return RecordEvents.Where(x => x.Bar == barIndex && x.IsMetronomeTick == false);
        }

        public RecordEvent GetOnOneFor(int barIndex)
        {
            return RecordEvents.FirstOrDefault(x => x.IsMetronomeTick && x.Bar == barIndex);
        }

        public void Clear()
        {
            RecordEvents.Clear();
        }

        public void InsertHits(Metronome metronome, MusicSheet musicSheet)
        {
            var barIndex = RecordEvents.Last(x => x.StartsFirstMusicBar).Bar;
         
            const double offset = 18d;
            double barWidth = 360.0d*musicSheet.Meter.Note/musicSheet.Meter.Count;
            double barDuration = musicSheet.GetBarDuration(metronome.BeatsPerMinute).TotalMilliseconds;
            foreach (var bar in musicSheet.Bars)
            {
                var ticks = GetTapsFor(barIndex);
                var onOne = GetOnOneFor(barIndex);
                var tickMarkers = ticks.Select(
                    t => new Tick {Left = offset + barWidth*t.Time.Subtract(onOne.Time).TotalMilliseconds/barDuration});

                bar.Ticks.Clear();
                foreach (var tickMarker in tickMarkers)
                {
                    bar.Ticks.Add(tickMarker);
                }
                barIndex++;
            }
        }

        public class RecordEvent
        {  
            public int Bar { get; set; }
            public int Tick { get; set; }

            public DateTime Time { get; set; }
            public TimeSpan Delay { get; set; }

            public bool IsMetronomeTick { get; set; }
            public bool StartsFirstMusicBar { get; set; }
        }
    }
}
