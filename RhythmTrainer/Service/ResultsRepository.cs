﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization;
using System.Threading.Tasks;
using RhythmTrainer.Models;
using RhythmTrainer.Models.Achievements;
using Windows.Storage;

namespace RhythmTrainer.Service
{
    public interface IResultsRepository
    {
        void Add(Results results);
        Task<List<Results>> GetAllResults();
        Task<ResultAggregate> GetResultAggregate();
        Task SaveResultAggregate(ResultAggregate aggregate);
    }

    public class ResultsRepository : IResultsRepository
    {
        private Type[] knownTypes = new Type[]
            {
                typeof(ResultAggregate),
                typeof(AchievementLine),
                typeof(AchievementLine.StateBase),
                typeof(TotalResultInRowAchievementLine), 
                typeof(TotalResultInRowAchievementLine.State),
                typeof(TempoAchievementLine),
                typeof(TempoAchievementLine.State),
            };

        public async void Add(Results results)
        {
            var allResults = await GetAllResults();
            allResults.Add(results);

            var folder = ApplicationData.Current.LocalFolder;
            var resultFile = await folder.CreateFileAsync("results.data", CreationCollisionOption.ReplaceExisting);

            using (var readWriteStream = await resultFile.OpenAsync(FileAccessMode.ReadWrite))
            {
                var stream = readWriteStream.AsStreamForWrite();
                var sessionSerializer = new DataContractSerializer(typeof (List<Results>), new Type[] {typeof (Results)});
                sessionSerializer.WriteObject(stream, allResults);
                await stream.FlushAsync();
            }
        }

        public async Task<List<Results>> GetAllResults()
        {
            var result = new List<Results>();
            var folder = ApplicationData.Current.LocalFolder;
            var resultFile = await folder.CreateFileAsync("results.data", CreationCollisionOption.OpenIfExists);

            using (var stream = await resultFile.OpenReadAsync())
            {
                var s1 = stream.AsStreamForRead();

                var sessionSerializer = new DataContractSerializer(typeof (List<Results>), new Type[] {typeof (Results)});
                while (s1.Position < s1.Length)
                {
                    try
                    {
                        result = (List<Results>) sessionSerializer.ReadObject(s1);
                    }
                    catch (Exception ex)
                    {
                    }
                }
            }
            return result;
        }

        public async Task<ResultAggregate> GetResultAggregate()
        {
            var folder = ApplicationData.Current.LocalFolder;
            var resultFile = await folder.CreateFileAsync("aggregation.data", CreationCollisionOption.OpenIfExists);

            using (var stream = await resultFile.OpenReadAsync())
            {
                var s1 = stream.AsStreamForRead();

                var sessionSerializer = new DataContractSerializer(typeof(ResultAggregate), knownTypes);
                while (s1.Position < s1.Length)
                {
                    try
                    {
                        return (ResultAggregate)sessionSerializer.ReadObject(s1);
                    }
                    catch (Exception ex)
                    {
                    }
                }
            }
            return new ResultAggregate();
        }

        public async Task SaveResultAggregate(ResultAggregate aggregate)
        {
            var folder = ApplicationData.Current.LocalFolder;
            var resultFile = await folder.CreateFileAsync("aggregation.data", CreationCollisionOption.ReplaceExisting);

            using (var readWriteStream = await resultFile.OpenAsync(FileAccessMode.ReadWrite))
            {
                var stream = readWriteStream.AsStreamForWrite();
                var sessionSerializer = new DataContractSerializer(typeof(ResultAggregate), knownTypes);
                sessionSerializer.WriteObject(stream, aggregate);
                await stream.FlushAsync();
            }
        }
    }
}
