﻿using System.Collections.Generic;
using RhythmTrainer.Models;

namespace RhythmTrainer.Service
{
    public class BarScafold
    {
        private IList<BarElement> _notes;
        private Rational _meter;
        private Rational _complete;

        public BarScafold(Meter meter)
        {
            _notes = new List<BarElement>();
            _complete = new Rational(0, 1); 
            _meter =  new Rational(meter.Note, meter.Count);
        }

        public Rational MeterLeft
        {
            get { return _meter - _complete; }
        }

        public bool IsComplete
        {
            get { return _meter.Equals( _complete); }
        }

        public IEnumerable<BarElement> Notes
        {
            get { return _notes; }
        }

        public void AddNote(BarElement note)
        {
            _complete += note.NoteFraction;
            _notes.Add(note);
        }
    }
}