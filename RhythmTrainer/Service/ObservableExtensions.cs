﻿using System;
using System.Reactive;
using System.Reactive.Concurrency;
using System.Reactive.Linq;
using Windows.Foundation.Collections;
using Windows.UI.Xaml.Media;

namespace RhythmTrainer.Service
{
    public static class ObservableExtensions
    {
        public static IObservable<T> DelayEachBy<T>(this IObservable<T> source, Func<T, TimeSpan> delaySelector,
            IScheduler scheduler)
        {
            return source.Select(x => Observable.Return(x).Delay(delaySelector(x), scheduler)).Concat();
        }

        public static IObservable<T> DelayNextBy<T>(this IObservable<T> source, Func<T, TimeSpan> delaySelector,
            IScheduler scheduler)
        {
            return source.Take(1).Concat(
                source.Skip(1)
                    .Zip(source, (x, d) => new {x, delay = delaySelector(d)})
                    .DelayEachBy(x => x.delay,scheduler)
                    .Select(x => x.x));
        }

        public static IObservable<T> EndOn<T>(this IObservable<T> source, int index)
        {
            return source.TakeWhile((x, i) => i < index);
        }

        public static IObservable<T> UntilNextAfter<T, TOther>(this IObservable<T> source, IObservable<TOther> other)
        {
            var bits = source.Select(x => 1).TakeUntil(other).Concat(Observable.Return(0));
            return source.Zip(bits, (s, b) => new {s, b}).TakeWhile(x => x.b == 1).Select(x => x.s);
        }

        public static IObservable<long> InvertedInterval(TimeSpan interval, IScheduler scheduler)
        {
            return Observable.Interval(interval, scheduler)
                .StartWith(-1L)
                .Select(x => x + 1);

        }

        public static IObservable<long> LoopFirstBarUntil<TStop>(this IObservable<long> source, IObservable<TStop> stop)
        {
            return source.UntilNextAfter(stop).Select(x => 0L);
        }

        public static IObservable<long> PlayMusicBars(this IObservable<long> source, int numberOfBars)
        {
            return source.Take(numberOfBars - 1).Select(x => x + 1L);
        }

        public static IObservable<MetronomeTick> InsertTicksIntoEachBar(this IObservable<long> bars,
            IObservable<long> ticks, int meterCount)
        {
            return bars.SelectMany(musicBar => ticks.Select(tick => new {tick, musicBar}))
                .Select((t, i) => new MetronomeTick(i/meterCount, (int) t.tick, (int) t.musicBar));
        }
    }
}