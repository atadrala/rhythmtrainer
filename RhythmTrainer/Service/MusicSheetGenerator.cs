﻿using System.Linq;
using RhythmTrainer.Models;
using RhythmTrainer.Service.Generators;

namespace RhythmTrainer.Service
{
    public interface IMusicSheetGenerator
    {
        MusicSheet Generate(GeneratorSchema generatorSchema, int barCount);
    }

    public class MusicSheetGenerator : IMusicSheetGenerator
    {
        private RoundRulette _roundRulette;
        private Meter _meter;

        public MusicSheet Generate(GeneratorSchema generatorSchema, int barCount)
        {
            InitializeRoundRulette(generatorSchema);
            var bars = Enumerable
                .Range(0, barCount)
                .Select(_ => GenerateBar())
                .ToList();

            return new MusicSheet
                {
                    Meter = _meter,
                    Bars = bars,
                };
        }

        private void InitializeRoundRulette(GeneratorSchema generatorSchema)
        {
            _meter = generatorSchema.Meter;
            _roundRulette = new RoundRulette();
            _roundRulette.SetPropability(new TimeRestrictedNoteGenerator<FullNote>(new Rational(1, 1)), generatorSchema.Probability<FullNote>());
            _roundRulette.SetPropability(new TimeRestrictedNoteGenerator<Half>(new Rational(1, 2)), generatorSchema.Probability<Half>());
            _roundRulette.SetPropability(new TimeRestrictedNoteGenerator<Quarter>(new Rational(1, 4)), generatorSchema.Probability<Quarter>());
            _roundRulette.SetPropability(new NoteGroupGenerator<NoteGroup>(new Rational(1, 4)), generatorSchema.Probability<NoteGroup>());
            _roundRulette.SetPropability(new NoteGroupGenerator<TrippleNoteGroup>(new Rational(1, 4)), generatorSchema.Probability<TrippleNoteGroup>());
            _roundRulette.SetPropability(new NoteGroupGenerator<SixteenGroup>(new Rational(1, 4)), generatorSchema.Probability<SixteenGroup>());
            _roundRulette.SetPropability(new NoneconsecutiveNoteGenerator<Eight>(new Rational(1, 8)), generatorSchema.Probability<Eight>());
            _roundRulette.SetPropability(new NoneconsecutiveNoteGenerator<QuarterRest>(new Rational(1, 4)), generatorSchema.Probability<QuarterRest>());
            _roundRulette.SetPropability(new NoneconsecutiveNoteGenerator<HalfRest>(new Rational(1, 2)), generatorSchema.Probability<HalfRest>());
            _roundRulette.SetPropability(new TimeRestrictedNoteGenerator<DottedNote<Half>>(new Rational(3, 4)), generatorSchema.Probability<DottedNote<Half>>());
            _roundRulette.SetPropability(new TimeRestrictedNoteGenerator<DottedNote<Quarter>>(new Rational(3, 8)), generatorSchema.Probability<DottedNote<Quarter>>());
            _roundRulette.SetPropability(new TimeRestrictedNoteGenerator<DottedNote<HalfRest>>(new Rational(3, 4)), generatorSchema.Probability<DottedNote<HalfRest>>());
            _roundRulette.SetPropability(new TimeRestrictedNoteGenerator<DottedNote<QuarterRest>>(new Rational(3, 8)), generatorSchema.Probability<DottedNote<QuarterRest>>());        
        }

        private Bar GenerateBar()
        {
            var barScafold = new BarScafold(_meter);
            while (barScafold.IsComplete == false)
            {
                var generator = _roundRulette.PickGeneratorFor(barScafold);
                barScafold.AddNote(generator.GenerateNotes());
            }

            return new Bar(barScafold.Notes, _meter);
        }
    }
}
