﻿using System;
using System.Collections.Generic;
using System.Linq;
using RhythmTrainer.Models;

namespace RhythmTrainer.Service
{
    class JudgeState
    {
        private readonly IEnumerator<Recorder.RecordEvent> recording;
        private DateTime start;
        private double totalError;
        private int missingNotes;
        private int extraHits;
        private double currentNoteTime;
        private double currentNoteLeftOffset;

        private const double Delta = 120.0d;
        private const double Epsilon = 50d;

        public JudgeState(Recorder recorder)
        {
            recording = recorder.RecordEvents.Where(x => x.IsMetronomeTick == false).GetEnumerator();
            recording.MoveNext();
           
            start = recorder.RecordEvents.Last(x=>x.StartsFirstMusicBar).Time;
          
            totalError = 0.0d;
            missingNotes = 0;
            extraHits = 0;
            currentNoteTime = 0.0d;
        }

        public int ExtraHits
        {
            get { return extraHits; }
        }

        public int MissingNotes
        {
            get { return missingNotes; }
        }

        public double TotalError
        {
            get { return totalError; }
        }

        public void MoveToNextClosestRecordedHit(BarElement note)
        {
            while (true)
            {
                if (recording.Current != null && recording.Current.Time.Subtract(start).TotalMilliseconds - currentNoteTime >= -Delta)
          {
                    return;
                } 
            
                recording.MoveNext();
                if (recording.Current != null)
                {
                    extraHits++;
                }
             
                if (recording.Current == null || recording.Current.Time.Subtract(start).TotalMilliseconds - currentNoteTime >= -Delta)
                {
                    return;
                } 
            }
        }

        public bool CheckIfFound(Bar bar)
        {
            if (recording.Current == null)
            {
                bar.NoteHits.Add(new NoteHit {Left = currentNoteLeftOffset, Hit = NoteHit.NoteHitType.Missing});
                missingNotes++;
                return false;
            }
            return true;
        }

        public bool CheckIfIsRest(BarElement note)
        {
            return note is Rest;
        }

        public void EstimateTimeDelay(Bar bar)
        {
            var tapTime = recording.Current.Time.Subtract(start).TotalMilliseconds;
            var noteHit = new NoteHit { Left = 10 + currentNoteLeftOffset };
            if (tapTime - currentNoteTime > Delta)
            {
                noteHit.Hit = NoteHit.NoteHitType.Missing;
                missingNotes++;
            }
            else if (tapTime - currentNoteTime < -Epsilon)
            {
                noteHit.Hit = NoteHit.NoteHitType.Early;
                totalError += Math.Abs(tapTime - currentNoteTime);
            }
            else if (tapTime - currentNoteTime > Epsilon)
            {
                noteHit.Hit = NoteHit.NoteHitType.Late;
                totalError += Math.Abs(tapTime - currentNoteTime);
            }
            else
            {
                noteHit.Hit = NoteHit.NoteHitType.Exact;
            }
            if (tapTime - currentNoteTime <= Delta)
            {
                recording.MoveNext();
            }
            bar.NoteHits.Add(noteHit);
        }

        public void UpdateCurentNoteState(BarElement note, MusicSheet musicSheet, Metronome metronome)
        {
            currentNoteTime += note.ToMiliseconds(musicSheet.Meter.Count, metronome.BeatsPerMinute); 
            currentNoteLeftOffset += note.Width;
        }

        public void CountLastExtraHits()
        {
            while (recording.Current != null)
            {
                if (recording.Current != null)
                {
                    extraHits++;
                }
                recording.MoveNext();
            }
        }

        public void ResetNoteOffset()
        {
            currentNoteLeftOffset = 0d;
        }
    }
}