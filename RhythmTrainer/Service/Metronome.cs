﻿using System;
using System.Reactive;
using System.Reactive.Concurrency;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Reactive.Subjects;
using RhythmTrainer.Models;

namespace RhythmTrainer.Service
{
    public class Metronome : IObservable<MetronomeTick>
    {
        private readonly IScheduler _scheduler;
        private readonly Subject<MetronomeTick> _tickSubject;
        private readonly SerialDisposable _metronome;
        private readonly Subject<bool> _aborted;
        private readonly Subject<Unit> _started;
        private readonly Subject<bool> _metronomEnded; 

        private bool _isAborted;

        public double BeatsPerMinute { get; set; }
        public int MeterCount { get; set; }
        public bool IsPlaying { get; private set; }
        public int OnOneMargin { get; set; }

        public IObservable<bool> MetronomEnded
        {
            get { return _metronomEnded; }
        }

        public Metronome(IScheduler scheduler)
        {
            _scheduler = scheduler;
            _tickSubject = new Subject<MetronomeTick>();
            _metronome = new SerialDisposable();
            _started = new Subject<Unit>();
            _aborted = new Subject<bool>();
            _metronomEnded = new Subject<bool>();

            _aborted.Subscribe(x => _isAborted = x);
        }

        public void Start(Meter meter, double bpm, int barsToPlay)
        {
            BeatsPerMinute = bpm;
            MeterCount = meter.Note;
            IsPlaying = true;
            _isAborted = false;

            var tickTime = TimeSpan.FromMilliseconds(60000.0d / (bpm));
            var barTime = TimeSpan.FromMilliseconds(60000.0d / (bpm) * meter.Note);

            var bars = ObservableExtensions.InvertedInterval(barTime, _scheduler);
            var ticks = ObservableExtensions.InvertedInterval(tickTime, _scheduler).EndOn(meter.Note);

            _metronome.Disposable =
                bars.LoopFirstBarUntil(_started)
                    .Concat(
                bars.PlayMusicBars(barsToPlay))
                    .InsertTicksIntoEachBar(ticks, MeterCount)
                    .TakeUntil(_aborted)
                    .Subscribe(t => _tickSubject.OnNext(t), OnEnd);
        }

        public void Stop()
        {
            _aborted.OnNext(true);
        }

        public void Play()
        {
            Observable
                .Return(Unit.Default)
                .Delay(TimeSpan.FromMilliseconds(OnOneMargin), _scheduler)
                .Subscribe(_started.OnNext);
        }

        protected virtual void OnEnd()
        {
            IsPlaying = false;
            _metronomEnded.OnNext(_isAborted == false);
        }

        public IDisposable Subscribe(IObserver<MetronomeTick> observer)
        {
            return _tickSubject.Subscribe(observer);
        }

        public IObservable<MetronomeTick> OnOne()
        {
            return this.Skip(MeterCount - 1)
                    .Take(1);
        }
    }

    public class MetronomeTick
    {
        public int Bar { get; private set; }
        public int Tick { get; private set; }
        public int MusicBar { get; private set; }
        public bool BeginsMusicSheet { get { return Bar == 0 && Tick == 0; } }

        public MetronomeTick(int bar, int tick)
        {
            Bar = bar;
            Tick = tick;
        }

        public MetronomeTick(int bar, int tick, int musicBar)
        {
            Bar = bar;
            Tick = tick;
            MusicBar = musicBar;
        }
    }
}
