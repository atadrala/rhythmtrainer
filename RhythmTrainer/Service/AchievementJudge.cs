﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using RhythmTrainer.Models;

namespace RhythmTrainer.Service
{
    public class AchievementJudge
    {
        private readonly IResultsRepository resultsRepository;
        private readonly IEnumerable<AchievementLine> achivementLines;

        public AchievementJudge(IResultsRepository resultsRepository, AchievementLine[] achivementLines)
        {
            this.resultsRepository = resultsRepository;
            this.achivementLines = achivementLines;
        }

        public async Task<IList<Achievement>> JudgeResult(Results results)
        {
            var aggregate = await resultsRepository.GetResultAggregate();

            var achievements = achivementLines
                .Select(x => x.Judge(results, aggregate))
                .Where(x => x != null)
                .ToList();

            aggregate.Update(results);
            await resultsRepository.SaveResultAggregate(aggregate);
            return achievements;
        }
    }

    public abstract class AchievementLine
    {
        public abstract string Message { get; }

        public abstract string Description { get; }

        public abstract Achievement Judge(Results results, ResultAggregate aggregate);
     
        public virtual StateBase CreateState()
        {
            return new StateBase();
        }
 
        public class StateBase
        {
            public int Level { get; set; }
        }
     }
}