﻿using System;
using System.Collections.Generic;
using System.Reactive;
using System.Reactive.Concurrency;
using System.Reactive.Linq;
using System.Reactive.Subjects;

namespace RhythmTrainer.Service
{
    public class RecordingsPlayer
    {
        private readonly ISoundPlayer _soundPlayer;
        private readonly Subject<Unit> _stoped;
        private readonly Subject<IEnumerable<Recorder.RecordEvent>> _player;
        private readonly IDisposable _playback;

        public RecordingsPlayer(ISoundPlayer soundPlayer, IScheduler scheduler)
        {
            _soundPlayer = soundPlayer;

            _stoped = new Subject<Unit>();
            _player = new Subject<IEnumerable<Recorder.RecordEvent>>();

            _playback =
                _player
                    .ObserveOn(scheduler)
                    .SelectMany(x => x)
                    .DelayEachBy(x => x.Delay, scheduler)
                    .TakeUntil(_stoped)
                    .Repeat()
                    .Subscribe(PlaySound);
        }

        public void Play(IList<Recorder.RecordEvent> recordings)
        {
            Stop();
            _player.OnNext(recordings);
        }

        public void Stop()
        {
            _stoped.OnNext(Unit.Default);
        }

        private void PlaySound(Recorder.RecordEvent recordEvent)
        {
            if (recordEvent.IsMetronomeTick && recordEvent.Tick == 0)
            {
                _soundPlayer.Clack();
            }
            else if (recordEvent.IsMetronomeTick && recordEvent.Tick != 0)
            {
                _soundPlayer.Click();
            }
            else
            {
                _soundPlayer.Tap();
            }
        }
    }
}