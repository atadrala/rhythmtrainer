﻿using System.Collections.Generic;
using SharpDX.IO;
using SharpDX.Multimedia;
using SharpDX.XAudio2;

namespace RhythmTrainer.Service
{
    public interface ISoundPlayer
    {
        void Tap();
        void Click();
        void Clack();
        void Play(MetronomeTick metronomeTick);
    }

    public class SoundPlayer : ISoundPlayer
    {
        private const string _clickName = "Assets/losticks.wav";
        private const string _clackName = "Assets/histicks.wav";
        private const string _tapName = "Assets/Snare2.wav";

        private readonly Dictionary<string, SourceVoice> _loadedSounds = new Dictionary<string, SourceVoice>();
        private readonly Dictionary<string, AudioBufferAndMetaData> _audioBuffers = new Dictionary<string, AudioBufferAndMetaData>();
  
        private MasteringVoice _masteringVoice;
        public MasteringVoice MasteringVoice
        {
            get
            {
                if (_masteringVoice == null)
                {
                    _masteringVoice = new MasteringVoice(XAudio);
                    _masteringVoice.SetVolume(1);
                }
                return _masteringVoice;
            }
        }
       
        private XAudio2 _xAudio2;
        public XAudio2 XAudio
        {
            get
            {
                if (_xAudio2 == null)
                {
                    _xAudio2 = new XAudio2();
                    var voice = MasteringVoice; //touch voice to create it
                    _xAudio2.StartEngine();
                }
                return _xAudio2;
            }
        }

        public SoundPlayer()
        {
            // Force loading of 
            PlaySound(_clickName, 0f);
            PlaySound(_clackName, 0f);
            PlaySound(_tapName, 0f);
        }

        public void Tap()
        {
            PlaySound(_tapName, 0.5f);
        }

        public void Click()
        {
            PlaySound(_clickName);
        }
        public void Clack()
        {
            PlaySound(_clackName);
        }

        public void Play(MetronomeTick metronomeTick)
        {
            if (metronomeTick.Tick == 0)
            {
                Clack();
            }
            else
            {
                Click();
            }
        }

        private void PlaySound(string soundfile, float volume = 1)
        {
            SourceVoice sourceVoice;
            if (!_loadedSounds.ContainsKey(soundfile))
            {

                var buffer = GetBuffer(soundfile);
                sourceVoice = new SourceVoice(XAudio, buffer.WaveFormat, true);
                sourceVoice.SetVolume(volume);
                sourceVoice.SubmitSourceBuffer(buffer, buffer.DecodedPacketsInfo);
                sourceVoice.Start();
            }
            else
            {
                sourceVoice = _loadedSounds[soundfile];
                if (sourceVoice != null)
                    sourceVoice.Stop();
            }
        }

        private AudioBufferAndMetaData GetBuffer(string soundfile)
        {
            if (!_audioBuffers.ContainsKey(soundfile))
            {
                var nativefilestream = new NativeFileStream(
                        soundfile,
                        NativeFileMode.Open,
                        NativeFileAccess.Read);

                var soundstream = new SoundStream(nativefilestream);

                var buffer = new AudioBufferAndMetaData
                    {
                    Stream = soundstream.ToDataStream(),
                    AudioBytes = (int)soundstream.Length,
                    Flags = BufferFlags.EndOfStream,
                    WaveFormat = soundstream.Format,
                    DecodedPacketsInfo = soundstream.DecodedPacketsInfo
                };
                _audioBuffers[soundfile] = buffer;
            }
            return _audioBuffers[soundfile];

        }
   
        private sealed class AudioBufferAndMetaData : AudioBuffer
        {
            public WaveFormat WaveFormat { get; set; }
            public uint[] DecodedPacketsInfo { get; set; }
        }
    }
}
