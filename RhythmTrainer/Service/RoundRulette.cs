﻿using System;
using System.Collections.Generic;
using System.Linq;
using RhythmTrainer.Service.Generators;

namespace RhythmTrainer.Service
{
    public class RoundRulette
    {
        private readonly Random _random;
        private readonly IList<INoteGenerator> _generators;
        private readonly IDictionary<INoteGenerator, double> _probabilities; 

        public RoundRulette()
        {
            _random = new Random(Guid.NewGuid().GetHashCode());
            _generators = new List<INoteGenerator>();
            _probabilities = new Dictionary<INoteGenerator, double>();
        }

        public void SetPropability(INoteGenerator noteGenerator, double probability)
        {
            _generators.Add(noteGenerator);
            _probabilities.Add(noteGenerator, probability);
        }

        public  INoteGenerator PickGeneratorFor(BarScafold barScafold)
        {
            var applicableGenerators = _generators.Where(g => g.CanGenerateFor(barScafold)).ToArray();
            var propabilitySum = 0.0d;
            var rand = _random.NextDouble() * applicableGenerators.Sum(x => _probabilities[x]);
            foreach (var generator in applicableGenerators)
            {
                if (_probabilities[generator] + propabilitySum > rand)
                {
                    return generator;
                }
                propabilitySum += _probabilities[generator];
            }
            throw new InvalidOperationException();
        }
    }
}