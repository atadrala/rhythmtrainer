﻿using System;
using System.Linq;
using System.Reactive;
using System.Reactive.Concurrency;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Reactive.Subjects;
using RhythmTrainer.Models;
using SharpDX.X3DAudio;

namespace RhythmTrainer.Service
{
    public class MusicPlayer
    {
        private readonly ISoundPlayer _soundPlayer;
        private readonly IScheduler _scheduler;
        private readonly IObservable<Unit> _stop;
        private readonly Subject<Unit> _playbackCompeted; 
        private readonly SerialDisposable _playback;

        public MusicPlayer(ISoundPlayer soundPlayer, IScheduler scheduler)
        {
            _soundPlayer = soundPlayer;
            _scheduler = scheduler;
            _stop = new Subject<Unit>();
            _playbackCompeted = new Subject<Unit>();
            _playback = new SerialDisposable();
        }

        public IObservable<Unit> PlaybackCompleted { get { return _playbackCompeted; } }

        public void Play(MusicSheet musicSheet, double bpm)
        {
            _playback.Disposable =
                new MusicPlayerInternal(musicSheet, bpm, _scheduler)
                .TakeUntil(_stop)
                .Subscribe(PlaySound, () => _playbackCompeted.OnNext(Unit.Default));
        }

        private void PlaySound(BarElement note)
        {
            if (note != null && !(note is Rest))
            {
                _soundPlayer.Tap();
            }
        }

        public void Stop()
        {
            _stop.Publish(Unit.Default);
            if (_playback.Disposable != null)
            {
                _playback.Disposable.Dispose();
            }
        }

        private class MusicPlayerInternal : IObservable<BarElement>
        {
            private readonly MusicSheet _musicSheet;
            private readonly double _bpm;
            private readonly IScheduler _scheduler;

            public MusicPlayerInternal(MusicSheet musicSheet, double bpm, IScheduler scheduler)
            {
                _musicSheet = musicSheet;
                _bpm = bpm;
                _scheduler = scheduler;
            }

            public IDisposable Subscribe(IObserver<BarElement> observer)
            {
                TimeSpan offset = TimeSpan.Zero; //TODO: OPTIMIZE Scheduling one note ahead

                foreach (var note in _musicSheet.Bars.SelectMany(x => x.AllNotes))
                {
                    var localNote = note;
                    _scheduler.Schedule(offset, () => observer.OnNext(localNote));
                    offset = offset.Add(TimeSpan.FromMilliseconds(note.ToMiliseconds(_musicSheet.Meter.Count, _bpm)));
                }
                _scheduler.Schedule(offset, observer.OnCompleted);
                return Disposable.Empty;
            }
        }
    }
}