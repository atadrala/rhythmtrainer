﻿using System;
using System.Collections.Generic;
using System.Reactive.Concurrency;
using System.Reflection;
using System.Threading.Tasks;
using Caliburn.Micro;
using Microsoft.Practices.Unity;
using RhythmTrainer.Models.Achievements;
using RhythmTrainer.Service;
using RhythmTrainer.Views;
using Windows.ApplicationModel.Activation;
using Windows.ApplicationModel.Background;
using Windows.UI.Xaml.Controls;

namespace RhythmTrainer
{
    public sealed partial class App
    {
        private IUnityContainer _container;

        public App()
        {
            InitializeComponent();
            _container = new UnityContainer();

        }

        protected override void Configure()
        {
            base.Configure();
            if (!_container.IsRegistered<IEventAggregator>())
            {
                _container.RegisterType<IEventAggregator, EventAggregator>(new ContainerControlledLifetimeManager());
            }
            _container
                .RegisterType<ISoundPlayer, SoundPlayer>()
                .RegisterType<IResultsRepository, ResultsRepository>()
                .RegisterType<IMusicSheetGenerator, MusicSheetGenerator>()
                .RegisterInstance(typeof(IScheduler), DefaultScheduler.Instance)
                        .RegisterType<AchievementLine, NoteCountAchievementLine>("NoteCountAchievementLine")
                        .RegisterType<AchievementLine, PerfectTotalResultsAchievementLine>("PerfectTotalResultsAchievementLine")
                        .RegisterType<AchievementLine, TotalResultInRowAchievementLine>("TotalResultInRowAchievementLine")
                        .RegisterType<AchievementLine, TempoAchievementLine>("TempoAchievementLine")
                .RegisterType<Metronome>(new InjectionProperty("OnOneMargin", 120));
        }

        protected override object GetInstance(Type service, string key)
        {
            if (string.IsNullOrWhiteSpace(key))
            {
                return _container.Resolve(service);
            }
            else
            {
                return _container.Resolve(service, key);
            }
        }

        protected override IEnumerable<object> GetAllInstances(Type service)
        {
            return _container.ResolveAll(service);
        }

        protected override void BuildUp(object instance)
        {
            _container.BuildUp(instance);
        }

        protected override void PrepareViewFirst(Windows.UI.Xaml.Controls.Frame rootFrame)
        {
            if (!_container.IsRegistered<INavigationService>())
            {
                _container.RegisterInstance<INavigationService>(new FrameAdapter(rootFrame, false));
            }
        }

        protected override async void OnLaunched(LaunchActivatedEventArgs args)
        {
            DisplayRootView<MenuView>();

            await RegisterBackgroundTask();
        }

        private async Task RegisterBackgroundTask()
        {
            //var result = await BackgroundExecutionManager.RequestAccessAsync();
            //if (result == BackgroundAccessStatus.AllowedMayUseActiveRealTimeConnectivity ||
            //    result == BackgroundAccessStatus.AllowedWithAlwaysOnRealTimeConnectivity)
            //{
            //    foreach (var task in BackgroundTaskRegistration.AllTasks)
            //    {
            //        if (task.Value.Name == "RhythmTrainer.Task")
            //            task.Value.Unregister(true);
            //    }

            //    BackgroundTaskBuilder builder = new BackgroundTaskBuilder();
            //    builder.Name = "RhythmTrainer.Task";
            //    builder.TaskEntryPoint = "RhythmTrainer.Tasks.LiveTileBackgroundTask";
            //    builder.SetTrigger(new TimeTrigger(15, false));

            //    var registration = builder.Register();
            //}
        }

        protected override void OnShareTargetActivated(ShareTargetActivatedEventArgs args)
        {
        }
    }
}
