﻿using System;
using System.Collections.ObjectModel;
using Caliburn.Micro;
using RhythmTrainer.Service;
using Windows.UI.Xaml.Controls;

namespace RhythmTrainer.ViewModels
{
    public class LevelSelectionViewModel : ViewModelBase
    {
        public ObservableCollection<LevelViewModel> Levels { get; private set; }

        public ObservableCollection<LevelGroupViewModel> LevelGroups { get; private set; }

        public Type Parameter { get; set; }

        public LevelSelectionViewModel(INavigationService navigationService)
            : base(navigationService)
        {
            LevelGroups = new ObservableCollection<LevelGroupViewModel>
            {
                new LevelGroupViewModel()
                    {
                        GroupName = "Group 1",
                        Description = "Lorem Ipsum lorem ipsum",
                        Levels =  new ObservableCollection<LevelViewModel>
                            {
                                new LevelViewModel {Title = "Full Notes and Halves", IsUnlocked = true, GeneratorSchema = GeneratorSchema.Halves },
                                new LevelViewModel {Title = "Full, halves and quarters", IsUnlocked = true, GeneratorSchema = GeneratorSchema.Quarters},
                                new LevelViewModel {Title = "Full, halves, quarters and double eights", IsUnlocked = false, GeneratorSchema = GeneratorSchema.EightsGroup},
                                new LevelViewModel {Title = "Full, halves, quadruple sixteenths", IsUnlocked = false, GeneratorSchema = GeneratorSchema.SixteenthGroup},
                                new LevelViewModel {Title = "Full, halves, quarters and eights", IsUnlocked = false, GeneratorSchema = GeneratorSchema.Eights},
                            }
                    },
                new LevelGroupViewModel()
                    {
                        GroupName = "Group 2",
                        Description = "Lorem Ipsum lorem ipsum",
                        Levels =  new ObservableCollection<LevelViewModel>
                            {                   
                                new LevelViewModel {Title = "Triples", IsUnlocked = false, GeneratorSchema = GeneratorSchema.Triples},
                                new LevelViewModel {Title = "Pauses", IsUnlocked = false, GeneratorSchema = GeneratorSchema.Pauses},
                                new LevelViewModel {Title = "Dotted notes", IsUnlocked = false, GeneratorSchema = GeneratorSchema.DottedNotes},
                            }
                    },
                
               new LevelGroupViewModel()
                    {
                        GroupName = "Group 3",
                        Description = "Lorem Ipsum lorem ipsum",
                        Levels =  new ObservableCollection<LevelViewModel>
                            {                   
                                new LevelViewModel {Title = "Triple meter", IsUnlocked = false, GeneratorSchema = GeneratorSchema.TripleMeter},
                                new LevelViewModel {Title = "2/4 meter", IsUnlocked = false, GeneratorSchema = GeneratorSchema.MarchMeter},
                                new LevelViewModel {Title = "5/4 meter", IsUnlocked = false, GeneratorSchema = GeneratorSchema.FiveFourthMeter},
                                new LevelViewModel {Title = "7/8 meter", IsUnlocked = false, GeneratorSchema = GeneratorSchema.AdvancedMeter},
                            }
                    }
                };
        }

        public void LevelSelected(ItemClickEventArgs levelCickedEventArgs)
        {
            var levelViewModel = (LevelViewModel)levelCickedEventArgs.ClickedItem;
            _navigationService.NavigateToViewModel(Parameter, levelViewModel);
        }
    }
    public class LevelGroupViewModel
    {
        public string GroupName { get; set; }
        public string Description { get; set; }
        public ObservableCollection<LevelViewModel> Levels { get; set; }
    }
}
