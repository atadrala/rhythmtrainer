﻿using System;
using System.Collections.Generic;
using Caliburn.Micro;
using RhythmTrainer.Models;
using Windows.Data.Xml.Dom;
using Windows.UI.Notifications;

namespace RhythmTrainer.ViewModels
{
    public class ChallengeViewModel : ViewModelBase
    {
        public ChallengeViewModel(INavigationService navigationService)
            : base(navigationService)
        {
            var content = TileUpdateManager.GetTemplateContent(TileTemplateType.TileSquareText01);

            XmlNodeList textNodes = content.GetElementsByTagName("text");

            textNodes[0].AppendChild(content.CreateTextNode(DateTime.Now.ToString()));

            var tileNotification = new TileNotification(content);
            var tileUpdateManager = TileUpdateManager.CreateTileUpdaterForApplication();
            tileUpdateManager.Update(tileNotification);

            Notes = new Bar(
                new List<BarElement>
                    {
                        new Eight(),
                        new Eight(),
                        new Eight(),
                        new Eight()
                    }, new Meter(2, 4));
        }

        public Bar Notes { get; set; }
    }
}
