﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Caliburn.Micro;
using PropertyChanged;
using RhythmTrainer.Models;
using RhythmTrainer.Service;

namespace RhythmTrainer.ViewModels
{
    [ImplementPropertyChanged]
    public class StatisticsViewModel : ViewModelBase
    {
        private readonly IResultsRepository _resultsRepository;

        public List<Results> Results { get; set; }

        public List<NameValueItem<string>> OverallScoreShares { get; set; }

        public List<NameValueItem<DateTime>> DailyProgress { get; set; }

        public List<NameValueItem<string>> LevelAverageScore { get; set; }

        public ResultAggregate AggregatedResults { get; set; }

        public StatisticsViewModel(INavigationService navigationService, IResultsRepository resultsRepository)
            : base(navigationService)
        {
            _resultsRepository = resultsRepository;
        }

        protected override async void OnActivate()
        {
            Results = await _resultsRepository.GetAllResults();
            var aggregatedResults = await _resultsRepository.GetResultAggregate();

            // Hack to enforce evaluation of PlayedExcercises and PlayedNotes based on results in case not aggregated from the start.
            if (aggregatedResults.PlayedExcercises == 0)
            {
                aggregatedResults.PlayedExcercises = Results.Count();
            }
            if (aggregatedResults.PlayedNotes == 0)
            {
                aggregatedResults.PlayedNotes = Results.Sum(x => x.TotalNotes - x.MissingNotes + x.ExtraHits);
            }
            AggregatedResults = aggregatedResults;
            // <-- Hack

            Results = Results.Where(x => x.DateTime > DateTime.Now.Subtract(TimeSpan.FromDays(30))).Reverse().ToList();

            OverallScoreShares = Results
                .GroupBy(x => x.Summary)
                .Select(g => new NameValueItem<string>
                 {
                     Name = g.Key,
                     Value = g.Count(),
                 }).ToList();

            DailyProgress = Results
                .GroupBy(x => x.DateTime.Date)
                .OrderBy(x => x.Key)
                .Select(g => new NameValueItem<DateTime>
                    {
                        Name = g.Key,
                        Value = (int) (100*g.Average(e => e.TotalResult)),
                    }).ToList();

            LevelAverageScore = Results
                .GroupBy(x => x.LevelTitle)
                .Select(g => new NameValueItem<string>
                    {
                        Name = g.Key,
                        Value = (int) (100*g.Average(e => e.TotalResult)),
                    }).ToList();
        }
    }

    public class NameValueItem<T>
    {
        public T Name { get; set; }
        public int Value { get; set; }
    }
}
