﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RhythmTrainer.ViewModels
{
    [AttributeUsage(AttributeTargets.Property,AllowMultiple = false)]
    public class PersistedPropertyAttribute : Attribute
    {
        public object DefaultValue { get; set; }
        public PersistedPropertyAttribute()
        {
        }
    }
}
