﻿using RhythmTrainer.Service;

namespace RhythmTrainer.ViewModels
{
    public class LevelViewModel
    {
        public string Title { get; set; }
        public string Subtitle { get; set; }
        public bool IsUnlocked { get; set; }
        public GeneratorSchema GeneratorSchema { get; set; }
    }
}