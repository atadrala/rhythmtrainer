﻿using System;
using System.Collections.Generic;
using System.Linq;
using Caliburn.Micro;
using PropertyChanged;
using RhythmTrainer.Models;
using RhythmTrainer.Service;

namespace RhythmTrainer.ViewModels
{
    [ImplementPropertyChanged]
    public class PracticeViewModel : ViewModelBase
    {
        private readonly ISoundPlayer _soundPlayer;
        private readonly IResultsRepository _resultRepository;
        private readonly RecordingsPlayer _recordingsPlayer;
        private readonly MusicPlayer _musicPlayer;

        private readonly Metronome _metronome;
        private readonly Recorder _recorder;
        private readonly Judge _judge;
        private readonly AchievementJudge _achievementJudge;
        private readonly IMusicSheetGenerator _musicGenerator;
    

        private bool _awaitNextChallenge;
        public LevelViewModel Parameter { get; set; }

        public Results Results { get; set; }

        public IList<Achievement> Achivements { get; set; }

        public bool IsAchievementEarned { get; set; }

        [PersistedProperty(DefaultValue = 80.0d)]
        public double BeatsPerMinute { get; set; }

        public string LevelTitle { get { return Parameter.Title; } }

        public bool ResultVisible { get; set; }

        public MusicSheet MusicSheet { get; set; }

        public string TapText { get; set; }

        public int CurrentBarNumber { get; set; }

        [PersistedProperty(DefaultValue = 4)]
        public int BarNumber { get; set; }

        public PracticeViewModel(INavigationService navigationService, ISoundPlayer soundPlayer, IResultsRepository resultsRepository,
            RecordingsPlayer recordingsPlayer, Metronome metronome, AchievementJudge achievementJudge, MusicPlayer musicPlayer, IMusicSheetGenerator musicGenerator)
            : base(navigationService)
        {
            _soundPlayer = soundPlayer;
            _resultRepository = resultsRepository;
            _recordingsPlayer = recordingsPlayer;
            _achievementJudge = achievementJudge;
            _musicPlayer = musicPlayer;
            _musicGenerator = musicGenerator;

            _recorder = new Recorder();
            _judge = new Judge();

            _metronome = metronome;
            _metronome.Subscribe(MetronomeTick);
            _metronome.MetronomEnded.Subscribe(MetronomeEnd);

            TapText = "Tap to start!";
        }

        public void Start()
        {
            _recorder.Clear();
            _metronome.Start(Parameter.GeneratorSchema.Meter, BeatsPerMinute, MusicSheet.Bars.Count());
        }

        private async void JudgeResults()
        {
            _recorder.ProcessRecording();
            _recorder.InsertHits(_metronome, MusicSheet);
            Results = _judge.JudgeRecording(_recorder, _metronome, MusicSheet);
            Achivements = await _achievementJudge.JudgeResult(Results);
            IsAchievementEarned = Achivements.Any();
        }

        public void Tap()
        {
            _recordingsPlayer.Stop();
            _musicPlayer.Stop();

            if (ResultVisible)
            {
                ResultPaneTapped();
                return;
            }
            if (_awaitNextChallenge)
            {
                GenerateMusicSheet();
                _awaitNextChallenge = false;
            }
            if (_metronome.IsPlaying)
            {
                _metronome.Play();
                _recorder.RecordTap();
                _soundPlayer.Tap();
            }
            else
            {
                TapText = "Tap!";
                Start();
            }
        }

        public void ResultPaneTapped()
        {
            ResultVisible = false;
            TapText = "Tap for next challenge!";
            _awaitNextChallenge = true;
        }

        private void MetronomeTick(MetronomeTick metronomeTick)
        {
            _soundPlayer.Play(metronomeTick);
            _recorder.RecordTick(metronomeTick);
            CurrentBarNumber = metronomeTick.MusicBar;
        }

        private void MetronomeEnd(bool finished)
        {
            CurrentBarNumber = -1;

            if (finished)
            {
                JudgeResults();
                Results.LevelTitle = Parameter.Title;
                _resultRepository.Add(Results);
                ResultVisible = true;
                NotifyOfPropertyChange(() => CanPlayRecordings);
            }
            TapText = "Tap to start!";
        }

        public void GenerateMusicSheet()
        {
            _metronome.Stop();
            _musicPlayer.Stop();
            _recordingsPlayer.Stop();
            CurrentBarNumber = -1;
            MusicSheet = _musicGenerator.Generate(Parameter.GeneratorSchema, BarNumber);

            TapText = "Tap to start!";
        }

        public void PlayRecordings()
        {
            _musicPlayer.Stop();
            _recordingsPlayer.Play(_recorder.RecordEvents);
        }

        public void PlayMusicsheet()
        {
            _recordingsPlayer.Stop();
            _musicPlayer.Play(MusicSheet, BeatsPerMinute);
        }

        public bool CanPlayRecordings
        {
            get { return _recorder.RecordEvents != null && _recorder.RecordEvents.Any() && _metronome.IsPlaying == false; }
        }

        protected override void OnDeactivate(bool close)
        {
            base.OnDeactivate(close);
            if (_metronome != null)
            {
                _metronome.Stop();
            }
            _musicPlayer.Stop();
            _recordingsPlayer.Stop();
        }

        protected override void OnActivate()
        {
            base.OnActivate();
            GenerateMusicSheet();
        }
    }
}
