﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using System.Threading;
using System.Threading.Tasks;
using Caliburn.Micro;
using PropertyChanged;
using ReactiveUI;
using RhythmTrainer.Models;
using RhythmTrainer.Service;

namespace RhythmTrainer.ViewModels
{
    [ImplementPropertyChanged]
    public class ListeningPracticeViewModel : ViewModelBase
    {
        private readonly ISoundPlayer _soundPlayer;
        private readonly MusicPlayer _musicPlayer;
        private readonly IMusicSheetGenerator _musicGenerator;
        private readonly RecordingsPlayer _recordingsPlayer;
        private readonly Metronome _metronome;
        private readonly Recorder _recorder;
        private readonly Judge _judge;
        private readonly AchievementJudge _achievementJudge;

        private readonly  CancellationTokenSource _cancelaionToken;
    
        public LevelViewModel Parameter { get; set; }

        public MusicSheet MusicSheet { get; set; }

        public bool IsMusicVisible { get; set; }

        public string TapText { get; set; }

        public int CurrentBarNumber { get; set; }

        public bool ResultVisible { get; set; }

        public string LevelTitle { get { return Parameter.Title; } }

        [PersistedProperty(DefaultValue = 2)]
        public int BarNumber { get; set; }

        [PersistedProperty(DefaultValue = 80.0d)]
        public double BeatsPerMinute { get; set; }

        public Results Results { get; set; }

        public ReactiveCommand Taps { get; set; }

        public bool CanPlayMusicsheet { get; set; }

        public bool CanPlayRecordings { get; set; }

        public IList<Achievement> Achivements { get; set; }

        public bool IsAchievementEarned { get; set; }


        public ListeningPracticeViewModel(INavigationService navigationService, ISoundPlayer soundPlayer, IMusicSheetGenerator musicGenerator,
            RecordingsPlayer recordingsPlayer, AchievementJudge achievementJudge, MusicPlayer musicPlayer, Metronome metronome)
            : base(navigationService)
        {
            _soundPlayer = soundPlayer;
            _musicPlayer = musicPlayer;
            _musicGenerator = musicGenerator;
            _recordingsPlayer = recordingsPlayer;
            _achievementJudge = achievementJudge;
            _metronome = metronome;
            IsMusicVisible = true;

            _recorder = new Recorder();
            _judge = new Judge();
            Taps = new ReactiveCommand();
            _cancelaionToken = new CancellationTokenSource();

            _metronome.Subscribe(MetronomeTick);
        }

        private async void JudgeResults()
        {
            _recorder.ProcessRecording();
            _recorder.InsertHits(_metronome, MusicSheet);
            Results = _judge.JudgeRecording(_recorder, _metronome, MusicSheet);
            Achivements = await _achievementJudge.JudgeResult(Results);
            IsAchievementEarned = Achivements.Any();
        }

        public async void ExcerciseWorkflow(CancellationToken cancellationToken)
        {
            while (cancellationToken.IsCancellationRequested == false)
            {
                await ExcercisePrompt();
                await StartExercisePlayback();
                if (await CaptureUserTaps())
                {
                    DisplayResults();
                }
            }
        }

        private async Task ExcercisePrompt()
        {
            TapText = "Tap to start";
           
            await Taps.Take(1);

            ResultVisible = false;
            IsMusicVisible = false;
            CanPlayMusicsheet = false;
            CanPlayRecordings = false;
            GenerateMusicSheet();
            _recorder.Clear();
            _musicPlayer.Stop();
        }

        private async Task StartExercisePlayback()
        {
            TapText = "Steady!";

            _metronome.Start(MusicSheet.Meter, BeatsPerMinute, MusicSheet.Bars.Count());

            using (_metronome.Subscribe(t => CurrentBarNumber = t.Bar - 1))
            using (_metronome.OnOne().Subscribe(_ => _musicPlayer.Play(MusicSheet, BeatsPerMinute)))
            {
                await _musicPlayer.PlaybackCompleted.Take(1);
            }
            CurrentBarNumber = -1;
        }

        private async Task<bool> CaptureUserTaps()
        {
            TapText = "Tap!";

            using (_metronome.Subscribe(t => CurrentBarNumber = t.MusicBar))
            using (Taps.Subscribe(UserNoteTap))
            {
                return await _metronome.MetronomEnded.Take(1);
            }
        }

        private void DisplayResults()
        {
            TapText = "Tap for next challendge";
            IsMusicVisible = true;
            ResultVisible = true;
            CanPlayMusicsheet = true;
            CanPlayRecordings = true;
            CurrentBarNumber = -1;

            JudgeResults();
            Results.LevelTitle = Parameter.Title;
        }

        private void UserNoteTap(object tap)
        {
            _metronome.Play();
            _recorder.RecordTap();
            _soundPlayer.Tap();
        }

        private void MetronomeTick(MetronomeTick metronomeTick)
        {
            _soundPlayer.Play(metronomeTick);
            _recorder.RecordTick(metronomeTick);
        }

        public void ResultPaneTapped()
        {
            ResultVisible = false;
        }

        public void PlayRecordings()
        {
            _musicPlayer.Stop();
            var barIndex = _recorder.RecordEvents.Last(x => x.StartsFirstMusicBar).Bar;

            _recordingsPlayer.Play(_recorder.RecordEvents.SkipWhile(x => x.Bar < barIndex).ToList());
        }

        public void PlayMusicsheet()
        {
            _recordingsPlayer.Stop();
            _musicPlayer.Play(MusicSheet, BeatsPerMinute);
        }

        public void GenerateMusicSheet()
        {
            _metronome.Stop();
            _musicPlayer.Stop();
            //  _recordingsPlayer.Stop();
            CurrentBarNumber = -1;
            MusicSheet = _musicGenerator.Generate(Parameter.GeneratorSchema, BarNumber);
        }

        protected override void OnActivate()
        {
            base.OnActivate();
            TapText = "Tap for next challendge";
            IsMusicVisible = false;
          
            new Task(() => ExcerciseWorkflow(_cancelaionToken.Token), _cancelaionToken.Token).Start();
        }

        protected override void OnDeactivate(bool close)
        {
            base.OnDeactivate(close);
            if (_metronome != null)
            {
                _metronome.Stop();
            }
            _musicPlayer.Stop();
            _cancelaionToken.Cancel(false);
        }
    }
}