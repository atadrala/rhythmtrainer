﻿using Caliburn.Micro;

namespace RhythmTrainer.ViewModels
{
    public class HowToViewModel : ViewModelBase
    {
        public HowToViewModel(INavigationService navigationService) : base(navigationService)
        {
        }
    }
}