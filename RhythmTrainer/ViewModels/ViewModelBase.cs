﻿using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Caliburn.Micro;
using Windows.Storage;

namespace RhythmTrainer.ViewModels
{
    public abstract class ViewModelBase : Screen
    {
        protected readonly INavigationService _navigationService;
        private IList<PropertyInfo> _persistedProperties;

        protected ViewModelBase(INavigationService navigationService)
        {
            _navigationService = navigationService;
            ExtractPersistedProperties();
        }

        public void GoBack()
        {
            _navigationService.GoBack();
        }

        public bool CanGoBack
        {
            get
            {
                return _navigationService.CanGoBack;
            }
        }

        protected override void OnActivate()
        {
            base.OnActivate();
            var container = GetContainer();

            foreach (var property in _persistedProperties)
            {
                if (container.Values.ContainsKey(property.DeclaringType.Name + "^" + property.Name))
                {
                    property.SetValue(this, container.Values[property.DeclaringType.Name + "^" + property.Name]);
                }
                else
                {
                    var attr = property.GetCustomAttribute<PersistedPropertyAttribute>();
                    property.SetValue(this, attr.DefaultValue);
                }
            }
        }

        protected override void OnDeactivate(bool close)
        {
            base.OnDeactivate(close);
            var container = GetContainer();
            foreach (var property in _persistedProperties)
            {
                container.Values[property.DeclaringType.Name + "^" + property.Name] = property.GetValue(this);
            }
        }

        private void ExtractPersistedProperties()
        {
            _persistedProperties = new List<PropertyInfo>();
            foreach (var property in GetType().GetRuntimeProperties())
            {
                var attr = property.GetCustomAttribute<PersistedPropertyAttribute>();
                if (attr != null)
                {
                    _persistedProperties.Add(property);
                }
            }
        }

        private ApplicationDataContainer GetContainer()
        {
            var localSettings = ApplicationData.Current.LocalSettings;
            return !localSettings.Containers.ContainsKey("AppSettings")
                       ? localSettings.CreateContainer("AppSettings", ApplicationDataCreateDisposition.Always)
                       : localSettings.Containers["AppSettings"];
        }
    }
}
