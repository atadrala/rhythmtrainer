﻿using System;
using System.Collections.Generic;
using System.Linq;
using Caliburn.Micro;
using PropertyChanged;
using RhythmTrainer.Models.Achievements;
using RhythmTrainer.Service;
using Windows.UI.Xaml.Controls;

namespace RhythmTrainer.ViewModels
{
    [ImplementPropertyChanged]
    public class MenuViewModel: ViewModelBase
    {
        public bool IsAchievementDetailsVisible { get; set; }

        public AchievementViewModel SelectedAchievement { get; set; }

        public IObservableCollection<MenuItemViewModel> MenuItems { get; set; }

        public IEnumerable<AchievementViewModel> Achievements { get; set; }

        [PersistedProperty(DefaultValue = 0)]
        public int VisitCount {get; set; }
      
        [PersistedProperty(DefaultValue = false)]
        public bool Rated { get; set; }

        public bool IsPromptForSupportVisible { get; set; }

        public MenuViewModel(INavigationService navigationService) : base(navigationService)
        {
            MenuItems = new BindableCollection<MenuItemViewModel>
                {
                    new MenuItemViewModel(){Title="Practice", NavigationViewModelType =  typeof(LevelSelectionViewModel), NavigationViewModelParam = typeof(PracticeViewModel)},
                    new MenuItemViewModel(){Title="Practice Listening", NavigationViewModelType = typeof(LevelSelectionViewModel), NavigationViewModelParam = typeof(ListeningPracticeViewModel)},
                    new MenuItemViewModel(){Title="Statistics", NavigationViewModelType =  typeof(StatisticsViewModel)},
                    new MenuItemViewModel(){Title="How to", NavigationViewModelType =  typeof(HowToViewModel)},
                };
        }

        protected override async void OnActivate()
        {
            base.OnActivate();

              var achievementLines = new AchievementLine[]
                {
                    new NoteCountAchievementLine(), 
                    new PerfectTotalResultsAchievementLine(), 
                    new TotalResultInRowAchievementLine(), 
                    new TempoAchievementLine(), 
                };
            var resultRepository = new ResultsRepository();
            var aggregate = await resultRepository.GetResultAggregate();

            Achievements =
                achievementLines
                    .Select(al => 
                        new AchievementViewModel { 
                            Title = al.Message,
                            Description = al.Description,
                            Points = Enumerable.Repeat(1, aggregate.AchievementState(al).Level)
                        })
                    .Where(a => a.Points.Any());

            IsAchievementDetailsVisible = false;
            VisitCount++;
            if (VisitCount > 10 && Rated == false)
            {
                VisitCount = 0;
                IsPromptForSupportVisible = true;
            }
        }

        public void MenuSelected(ItemClickEventArgs levelCickedEventArgs)
        {
            var menuItem = (MenuItemViewModel)levelCickedEventArgs.ClickedItem;
            _navigationService.NavigateToViewModel(menuItem.NavigationViewModelType, menuItem.NavigationViewModelParam);
        }   

        public void AchievementSelected(ItemClickEventArgs achievementClickedEventArgs)
        {
            SelectedAchievement = (AchievementViewModel)achievementClickedEventArgs.ClickedItem;
            IsAchievementDetailsVisible = true;
        }

        public void AchievementDetailsTapped()
        {
            IsAchievementDetailsVisible = false;
        }

        public void RemindMeLater()
        {
            IsPromptForSupportVisible = false;
        }

        public void RateOrComment()
        {
            IsPromptForSupportVisible = false;
            Windows.System.Launcher.LaunchUriAsync(new Uri("ms-windows-store:REVIEW?PFN=14712ArturTadraa.RhythmTrainer_hrp0haw06pqwy"));
            Rated = true;
        }

        public void NoThanks()
        {
            IsPromptForSupportVisible = false;
            Rated = true;
        }
    }

    public class AchievementViewModel
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public IEnumerable<int> Points { get; set; }
    }

    public class MenuItemViewModel
    {
        public string Title { get; set; }
        public string  Subtitle { get; set; }
        public Type NavigationViewModelType { get; set; }
        public Type NavigationViewModelParam { get; set; }
    }
}
